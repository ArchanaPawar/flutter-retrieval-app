import 'package:flutter/material.dart';
import 'package:retrivalapp/ui/order_details/OrderDetailsNew/OrderDeatilHome.dart';
import 'ui/splash/SplashActivity.dart';
void main() {
  runApp(MyApp());
}
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Retrieval',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
       home: SplashActivity(),
       //home: OrderDetailHome(),
    );
  }
}

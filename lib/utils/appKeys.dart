import 'package:flutter/cupertino.dart';
import 'package:retrivalapp/utils/Fonts.dart';

import 'AppColor.dart';

Widget heightBox(double h) {
  return SizedBox(height: h);
}

double screenWidth(BuildContext context) {
  return MediaQuery.of(context).size.width;
}

screenHeight(BuildContext context) {
  return MediaQuery.of(context).size.height;
}

//textInputDecoration() {
//  return BoxDecoration(
//      color: colorGreyLight,
//      borderRadius: BorderRadius.all(Radius.circular(10)));
//}

//Widget headerText(String text) {
//  return Container(
//      margin: EdgeInsets.only(left: 5, bottom: 0),
//      child: Text(text,
//          style: TextStyle(
//              color: colorBlack,
//              fontSize: 16,
//              fontWeight: FontWeight.bold,
//              fontFamily: Fonts.kfont)));
//}

//Widget buttonText(String text) {
//  return Container(
//      margin: EdgeInsets.only(left: 15, bottom: 8),
//      child: Text(text,
//          style: TextStyle(
//              color: colorWhite,
//              fontSize: 16,
//              fontWeight: FontWeight.bold,
//              fontFamily: Fonts.kfont)));
//}

//hintStyle() {
//  return TextStyle(color: colorGrey, fontSize: 16, fontFamily: Fonts.kfont);
//}

//buttonDecoration(double radius) {
//  return BoxDecoration(
//    color: signinButtonColor,
//    borderRadius: BorderRadius.all(Radius.circular(radius)),
//  );
//}

buttonShape(double radius) {
  return RoundedRectangleBorder(
    borderRadius: BorderRadius.all(Radius.circular(radius)),
  );
}

//appBarTitle(String title) {
//  return Text(
//    title,
//    style: TextStyle(
//        color: colorBlack,
//        fontFamily: Fonts.kfont,
//        fontWeight: FontWeight.bold,
//        fontSize: 20),
//  );
//}
class APIConstant
{
  // Test environment url
  static const String BASE_URL = "http://tekdev.tekzee.in/cocoapi.ondoor.com/v1/";
  // Live environment url
  //  static const String BASE_URL = "https://cocoapi.ondoor.com/v1/";
  static const String validateAppVersion = BASE_URL + "user/Login/checkVersion";
  static const String OrderDone = BASE_URL + "onlineorders/Ordersretrieval/product_retrieval";
  static const String login = BASE_URL + "user/Login/retrievalLogin";
  static const String LockOrder = BASE_URL + "onlineorders/Ordersretrieval/assignorders";
  static const String updateProcureStatus = BASE_URL + "onlineorders/shortstock/updateProcureStatus";
  static const String getStoreList = BASE_URL + "user/Login/getcocolist";
  static const String getPrimaryCocoList = BASE_URL + "user/Login/getPrimarycocolist";
  static const String getRetrievalMenu = BASE_URL + "user/Login/retrieval";
  static const String ordersList = BASE_URL + "onlineorders/Ordersretrieval/ordersList";
  static const String getProductInOrder = BASE_URL + "onlineorders/ordersretrieval/getorderdetailforret/format/json/";
  static const String getProductInOrder_Pending = BASE_URL + "onlineorders/shortstock/getshortorderqty/format/json/MHL/";
  static const String getProductInOrder_PendingStockView = BASE_URL + "onlineorders/shortstock/getViewStock/format/json/MHL/";
  //
}
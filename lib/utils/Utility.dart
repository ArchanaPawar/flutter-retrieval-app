

import 'dart:io';
import 'dart:developer' as dev;
import 'package:flutter/material.dart';
import 'package:retrivalapp/utils/Constants.dart';
import 'package:retrivalapp/utils/AppColor.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Utility {
  DialogClickInterface interface;

  static int getDeviceType() {
    if (Platform.isIOS) {
      print('is a IOS');
      return 2;
    } else {
      print('is a Andriod');
      return 1;
    }
  }

  static String validateFirstName(String value) {
    String patttern = r'(^[a-zA-Z ]*$)';
    RegExp regExp = new RegExp(patttern);
    if (value.length == 0) {
      return "First Name Required";
    } else if (value.toLowerCase() == "null") {
      return "Frist name not be null";
    } else if (!regExp.hasMatch(value)) {
      return "First must be only Alphabatical";
    }
    return null;
  }

  static String validateLastName(String value) {
    String patttern = r'(^[a-zA-Z ]*$)';
    RegExp regExp = new RegExp(patttern);
    if (value.length == 0) {
      return "Last name Required";
    } else if (!regExp.hasMatch(value)) {
      return "Last Name must be Alphabatical";
    }
    return null;
  }

  static String validateEmail(String value) {
    String pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      return "Please Enter Email";
    } else if (!regExp.hasMatch(value)) {
      return "Invalid Email";
    } else {
      return null;
    }
  }

  static void ackAlert(BuildContext context, String message) {
    showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(Constants.appName),
          content: Text(message),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  static void customAlert(
      BuildContext context, String message, DialogClickInterface interface) {
    showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(Constants.appName),
          content: Text(message),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
                interface.onOkClick();
              },
            ),
          ],
        );
      },
    );
  }

  static showAlertDialog(BuildContext context,String message){

    Widget yesButton = FlatButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.of(context).pop();

      },
    );

    AlertDialog alert = AlertDialog(
      content: Text(message),
      actions: [
        yesButton,
      ],
    );

    /**
     * show the dialog
     */
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

//  static void showAlertDialog(BuildContext context, String message) {
//    showDialog<void>(
//      context: context,
//      builder: (BuildContext context) {
//        return AlertDialog(
//          title: Text(
//            Constants.appName,
//            textAlign: TextAlign.center,
//            style: TextStyle(
//                color: colorBlack,
//                fontFamily: Fonts.kfont,
//                fontWeight: FontWeight.bold,
//                fontSize: 18),
//          ),
//          contentPadding: EdgeInsets.all(0),
//          buttonPadding: EdgeInsets.all(0.0),
//          shape: RoundedRectangleBorder(
//              borderRadius: BorderRadius.all(Radius.circular(10))),
//          content: Container(
//            child: Wrap(
//              children: <Widget>[
//                Container(
//                  padding: EdgeInsets.all(20.0),
//                  child: Text(
//                    "$message",
//                    style: TextStyle(
//                        color: colorBlack,
//                        fontFamily: Fonts.kfont,
//                        fontSize: 16),
//                  ),
//                ),
//                Container(
////                  width: screenWidth(context),
//                  height: 50,
//                  decoration: BoxDecoration(
//                    color: Colors.transparent,
//                    border: Border(
//                      top: BorderSide(color: colorGreyLight, width: 1),
//                    ),
//                  ),
//                  child: Row(
//                    mainAxisSize: MainAxisSize.max,
//                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                    children: <Widget>[
//                      FlatButton(
//                        onPressed: () {
//                          Navigator.pop(context);
//                        },
//                        child: Text(
//                          "CANCEL",
//                          style: TextStyle(
//                              color: colorBlack,
//                              fontFamily: Fonts.kfont,
//                              fontSize: 16),
//                        ),
//                      ),
//                      Container(
//                        width: 1,
//                        color: colorGreyLight,
//                        height: 50,
//                      ),
//                      FlatButton(
//                        onPressed: () {
//                          Navigator.pop(context);
////                          logOut(context);
//                        },
//                        child: Text(
//                          "OK",
//                          style: TextStyle(
//                              color: colorPrimary,
//                              fontFamily: Fonts.kfont,
//                              fontSize: 16),
//                        ),
//                      ),
//                    ],
//                  ),
//                )
//              ],
//            ),
//          ),
//          actionsPadding: EdgeInsets.all(0),
//          /* actions: <Widget>[
//            Container(
//              width: screenWidth(context),
//              height: 50,
//              decoration: BoxDecoration(
//                  color: colorPrimary,
//                  border: Border(
//                    top: BorderSide(color: colorGrey, width: 5),
//                  ),
//                  borderRadius:
//                      BorderRadius.vertical(bottom: Radius.circular(10))),
//              child: Row(
//                children: <Widget>[
//                  FlatButton(
//                    onPressed: () {
//                      Navigator.pop(context);
//                    },
//                    child: Text(
//                      "CANCEL",
//                      style: TextStyle(
//                          color: colorBlack,
//                          fontFamily: Fonts.kfont,
//                          fontSize: 16),
//                    ),
//                  ),
//                  FlatButton(
//                    onPressed: () {
//                      Navigator.pop(context);
//                    },
//                    child: Text(
//                      "LOGOUT",
//                      style: TextStyle(
//                          color: colorPrimary,
//                          fontFamily: Fonts.kfont,
//                          fontSize: 16),
//                    ),
//                  ),
//                ],
//              ),
//            ),
//          ],*/
//        );
//      },
//    );
//  }

//  static void showAlertLoginSignUp(
//      BuildContext context, String message, LoginRegisterListener interface) {
//    showDialog<void>(
//      context: context,
//      builder: (BuildContext context) {
//        return AlertDialog(
//          title: Text(
//            Constants.appName,
//            textAlign: TextAlign.center,
//            style: TextStyle(
//                color: colorBlack,
//                fontFamily: Fonts.kfont,
//                fontWeight: FontWeight.bold,
//                fontSize: 18),
//          ),
//          contentPadding: EdgeInsets.all(0),
//          buttonPadding: EdgeInsets.all(0.0),
//          shape: RoundedRectangleBorder(
//              borderRadius: BorderRadius.all(Radius.circular(10))),
//          content: Container(
//            child: Wrap(
//              children: <Widget>[
//                Container(
//                  padding: EdgeInsets.all(20.0),
//                  child: Text(
//                    "$message",
//                    style: TextStyle(
//                        color: colorBlack,
//                        fontFamily: Fonts.kfont,
//                        fontSize: 16),
//                  ),
//                ),
//                Container(
////                  width: screenWidth(context),
//                  height: 50,
//                  decoration: BoxDecoration(
//                    color: Colors.transparent,
//                    border: Border(
//                      top: BorderSide(color: colorGreyLight, width: 1),
//                    ),
//                  ),
//                  child: Row(
//                    mainAxisSize: MainAxisSize.max,
//                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                    children: <Widget>[
//                      FlatButton(
//                        onPressed: () {
//                          Navigator.pop(context);
//                          interface.onRegisterClick();
//                        },
//                        child: Text(
//                          "Register",
//                          style: TextStyle(
//                              color: colorBlack,
//                              fontFamily: Fonts.kfont,
//                              fontSize: 16),
//                        ),
//                      ),
//                      Container(
//                        width: 1,
//                        color: colorGreyLight,
//                        height: 50,
//                      ),
//                      FlatButton(
//                        onPressed: () {
//                          Navigator.pop(context);
//                          interface.onLoginClick();
//                        },
//                        child: Text(
//                          "Login",
//                          style: TextStyle(
//                              color: colorPrimary,
//                              fontFamily: Fonts.kfont,
//                              fontSize: 16),
//                        ),
//                      ),
//                    ],
//                  ),
//                )
//              ],
//            ),
//          ),
//          actionsPadding: EdgeInsets.all(0),
//          /* actions: <Widget>[
//            Container(
//              width: screenWidth(context),
//              height: 50,
//              decoration: BoxDecoration(
//                  color: colorPrimary,
//                  border: Border(
//                    top: BorderSide(color: colorGrey, width: 5),
//                  ),
//                  borderRadius:
//                      BorderRadius.vertical(bottom: Radius.circular(10))),
//              child: Row(
//                children: <Widget>[
//                  FlatButton(
//                    onPressed: () {
//                      Navigator.pop(context);
//                    },
//                    child: Text(
//                      "CANCEL",
//                      style: TextStyle(
//                          color: colorBlack,
//                          fontFamily: Fonts.kfont,
//                          fontSize: 16),
//                    ),
//                  ),
//                  FlatButton(
//                    onPressed: () {
//                      Navigator.pop(context);
//                    },
//                    child: Text(
//                      "LOGOUT",
//                      style: TextStyle(
//                          color: colorPrimary,
//                          fontFamily: Fonts.kfont,
//                          fontSize: 16),
//                    ),
//                  ),
//                ],
//              ),
//            ),
//          ],*/
//        );
//      },
//    );
//  }

  static log(var tag, var message) {
    dev.log('\n\n*****************\n$tag\n$message\n*****************\n\n');
  }

//  static appBar(String title) {
//    return AppBar(
//      backgroundColor: appBarColor,
//      leading: IconButton(
//        onPressed: () {},
//        iconSize: 25,
//        icon: Image(
//          image: AssetImage(Images.menu),
//          width: 25,
//        ),
//      ),
//      elevation: 1,
//      centerTitle: true,
//      title: Text(
//        title,
//        style: TextStyle(
//            color: colorBlack,
//            fontSize: 25,
//            fontWeight: FontWeight.bold,
//            fontFamily: Fonts.kfont),
//      ),
//      actions: <Widget>[
//        IconButton(
//          onPressed: () {},
//          iconSize: 25,
//          icon: Image(
//            image: AssetImage(Images.searchImage),
//            width: 20,
//          ),
//        ),
//        SizedBox(
//          width: 15,
//        )
//      ],
//    );
//  }

  static Widget buildProgressIndicator(bool isLoading) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: new Center(
        child: new Opacity(
          opacity: isLoading ? 1.0 : 00,
          child: CircularProgressIndicator(
            strokeWidth: 3,
            valueColor: AlwaysStoppedAnimation<Color>(colorPrimary),
          ),
        ),
      ),
    );
  }

  static Future<String> getStringPreference(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(key) ?? "";
  }

  static Future<bool> setStringPreference(String key, String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(key, value);
  }

  static Future<int> getIntgerPreference(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(key) ?? 0;
  }

  static Future<bool> setIntegerPreference(String key, int value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setInt(key, value);
  }

  static Future<bool> setBooleanPreference(String key, bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool(key, value);
  }

  static Future<bool> getBooleanPreference(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(key) ?? false;
  }

  static Future<void> clearSharedPreference(BuildContext context) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }

  static bool equalsIgnoreCase(String string1, String string2) {
    return string1?.toLowerCase() == string2?.toLowerCase();
  }
}

abstract class DialogClickInterface {
  void onOkClick();
}

abstract class LoginRegisterListener {
  void onLoginClick();
  void onRegisterClick();
}


import 'package:flutter/material.dart';

const colorPrimary = const Color(0xFFc01414);
const colorPrimaryDark = const Color(0xFF941010);
const colorAccent = const Color(0xFFFFFFFF);
const menuButtonColor = const Color(0xFFf95d5d);

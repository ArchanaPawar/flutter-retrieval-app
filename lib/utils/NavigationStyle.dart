import 'package:flutter/material.dart';

// MARK:- Class For Push Animation With Rotation

class RotateRoute extends PageRouteBuilder {
  final Widget page;
  RotateRoute({this.page})  : super (
    pageBuilder: ( BuildContext context,  Animation<double> animation, Animation<double> secondaryAnimation,) =>
    page, transitionDuration: Duration(seconds: 1), transitionsBuilder: (
      BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child, ) =>
      ScaleTransition(
        scale: Tween<double>(
          begin: 0.0,
          end: 1.0,
        ).animate(
          CurvedAnimation(
            parent: animation,
            curve: Curves.fastOutSlowIn,
          ),
        ),
        child: RotationTransition(
          turns: Tween<double>(
            begin: 0.0,
            end: 1.0,
          ).animate(
            CurvedAnimation(
              parent: animation,
              curve: Curves.linear,
            ),
          ),
          child: child,
        ),
      ),
  );
}

class RotationRoute extends PageRouteBuilder {
  final Widget page;
  RotationRoute({this.page}) : super(
    pageBuilder: (BuildContext context,Animation<double> animation,Animation<double> secondaryAnimation,) =>
    page, transitionDuration: Duration(seconds: 1), transitionsBuilder: (BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child, ) =>
      RotationTransition(
        turns: Tween<double>(
          begin: 0.0,
          end: 1.0,
        ).animate(
          CurvedAnimation(
            parent: animation,
            curve: Curves.linear,
          ),
        ),
        child: child,
      ),
  );
}

class SizeRoute extends PageRouteBuilder {
  final Widget page;
  SizeRoute({this.page})
      : super(
    pageBuilder: (
        BuildContext context,
        Animation<double> animation,
        Animation<double> secondaryAnimation,
        ) =>
    page,
    transitionsBuilder: (
        BuildContext context,
        Animation<double> animation,
        Animation<double> secondaryAnimation,
        Widget child,
        ) =>
        Align(
          child: SizeTransition(
            sizeFactor: animation,
            child: child,
          ),
        ),
  );
}
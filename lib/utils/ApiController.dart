import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:http/io_client.dart';

import 'package:retrivalapp/utils/Utility.dart';
import 'Constants.dart';
import 'http.dart';
import 'package:http/http.dart' as http;
class ApiController
{
  var tag = 'ApiController';
  static ApiController _instance = new ApiController.internal();
  ApiController.internal();
  factory ApiController() {
    return _instance;
  }

  static ApiController getInstance() {
    if (_instance == null) {
      _instance = new ApiController.internal();
    }
    return _instance;
  }

  //https://stackoverflow.com/questions/58399365/flutter-http-request-send-using-xml-body
  Future<String> postXmlData(String url, String input) async {
    Utility.log(tag, "Api Call :\n $url \n Inputs :\n ${input.toString()}");
    var client = IOClient();
    var request = http.Request('POST', Uri.parse(url));
    request.headers.addAll({'content-type': 'text/xml'});
    request.bodyBytes = utf8.encode(input);
    var streamedResponse = await client.send(request);
    var responseBody =
        await streamedResponse.stream.transform(utf8.decoder).join();
    client.close();
    return responseBody;
  }

  Future<String> postXmlDataWithHeader(String url, String input) async {
    Utility.log(tag, "Api Call :\n $url \n Inputs :\n ${input.toString()}");
    var client = IOClient();
    var request = http.Request('POST', Uri.parse(url));
    request.headers.addAll(
        {
          'token':await Utility.getStringPreference(Constants.ACCESS_TOKEN),
          'content-type': 'text/xml'
        });
    request.bodyBytes = utf8.encode(input);
    var streamedResponse = await client.send(request);
    var responseBody =
    await streamedResponse.stream.transform(utf8.decoder).join();
    client.close();
    return responseBody;
  }

  Future<String> gets(String url) async {
    Utility.log(tag,"Api Call :\n $url ");
    var client = IOClient();
    var request = http.Request('GET', Uri.parse(url));
    request.headers.addAll(
        {
          'token':await Utility.getStringPreference(Constants.ACCESS_TOKEN),
          'request_type':'application'
        });

    var streamedResponse = await client.send(request);
    var responseBody = await streamedResponse.stream.transform(utf8.decoder).join();
    client.close();
    return responseBody;
  }
  Future<http.Response> getsNew(String url) async {
    Utility.log(tag,"Api Call :\n $url ");

    Map data = {
      'apikey': '12345678901234567890'
    };
    //encode Map to JSON
    var body = json.encode(data);
    String token=await Utility.getStringPreference(Constants.ACCESS_TOKEN);
    Utility.log("token", token);
    var response = await http.get(url,
        headers: {
          'token':await Utility.getStringPreference(Constants.ACCESS_TOKEN),
          'request_type':'application'},
    );
    print("${response.statusCode}");
    Utility.log("Api Response","${response.body}");
    return response;

  }
Future<http.Response> PostsNew(String url,var body) async {
    Utility.log(tag,"Api Call :\n $url ");
    Utility.log(tag, "Responsevaljson: " + body.toString());
    var response = await http.post(url,
        body: body,
        headers: {
          'token':await Utility.getStringPreference(Constants.ACCESS_TOKEN),
          'request_type':'application'},);
    print("${response.statusCode}");
    print("${response.body}");
    return response;

  }
}


import 'package:flutter/material.dart';

class Constants {
  static const appName = "Retrieval";
  static const customlabelFont = 18.0;
  static const objectViewBorder = 20.0;
  static const customfontweight = FontWeight.w500;

  static const IS_LOGIN = "IS_LOGIN";
  static const USER_ID = "USER_ID";
  static const EMPLOYEE_ID = "EMPLOYEE_ID";
  static const NAME = "NAME";

  static const STORE_ID = "STORE_ID";
  static const STORE_CODE = "STORE_CODE";
  static const WMS_STORE_ID = "WMS_STORE_ID";
  static const STORE_NAME = "STORE_NAME";
  static const LOCATION_ID = "LOCATION_ID";

  static const PRIMARY_STORE_ID = "PRIMARY_STORE_ID";
  static const PRIMARY_STORE_CODE = "PRIMARY_STORE_CODE";
  static const PRIMARY_WMS_STORE_ID = "PRIMARY_WMS_STORE_ID";
  static const PRIMARY_STORE_NAME = "PRIMARY_STORE_NAME";
  static const PRIMARY_LOCATION_ID = "PRIMARY_LOCATION_ID";

  static const ORDER_ID = "ORDER_ID";
  static const ORDER_DATE = "ORDER_DATE";
  static const ACCESS_TOKEN = "ACCESS_TOKEN";
  static const IS_ORDER_LOCK = "IS_ORDER_LOCK";

}
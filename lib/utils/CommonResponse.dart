import 'dart:convert';

class CommonResponse {
  String message;
  String status;

  CommonResponse({
    this.message,
    this.status,
  });

  factory CommonResponse.fromJson(String str) =>
      CommonResponse.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory CommonResponse.fromMap(Map<String, dynamic> json) => CommonResponse(
    message: json["message"] == null ? null : json["message"],
    status: json["status"] == null ? null : json["status"],
  );

  Map<String, dynamic> toMap() => {
    "message": message == null ? null : message,
    "status": status == null ? null : status,
  };
}

import 'package:retrivalapp/ui/primary_coco/PrimaryCocoResonse.dart';

abstract class PrimaryCocoView{
  void onFailed(String errorMessage);
  void populateStoreList(List<Datum> data);
}
import 'package:retrivalapp/utils/APIConstant.dart';
import 'package:retrivalapp/utils/ApiController.dart';
import 'package:retrivalapp/utils/Constants.dart';
import 'package:retrivalapp/utils/Message.dart';
import 'package:retrivalapp/utils/NetworkCheck.dart';
import 'package:retrivalapp/utils/Utility.dart';

import 'PrimaryCocoResonse.dart';
import 'PrimaryCocoView.dart';
class PrimaryCocoPresenter{
  final String TAG = "PrimaryCocoPresenter";
  PrimaryCocoView view;
  PrimaryCocoPresenter(this.view);
  ApiController apiController = new ApiController.internal();
  void getStoreList() async {
    if (await NetworkCheck.check()) {
      var xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
          "<user_info>\n " +
          "<user_id>" + await Utility.getStringPreference(Constants.EMPLOYEE_ID) + "</user_id>\n" +
          "</user_info>";
      apiController.postXmlDataWithHeader(APIConstant.getPrimaryCocoList, xml).then((String apiResponse) {
        Utility.log(TAG, "Response: " + apiResponse);
        PrimaryCocoResponse response = PrimaryCocoResponse.fromJson(apiResponse);
        if (response.status)
        {
          view.populateStoreList(response.data);
        } else {
          view.onFailed(response.message);
        }
      }).catchError((error) {
        Utility.log(TAG, error.toString());
        view.onFailed(Message.SOMETHING_WENT_WRONG);
      });
    } else {
      view.onFailed(Message.NO_INTERNET);
    }
  }
}
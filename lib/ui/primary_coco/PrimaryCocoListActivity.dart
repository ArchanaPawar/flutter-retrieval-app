
import 'package:flutter/material.dart';
import 'package:retrivalapp/ui/home/HomeActivity.dart';
import 'package:retrivalapp/ui/primary_coco/PrimaryCocoView.dart';
import 'package:retrivalapp/utils/AppColor.dart';
import 'package:retrivalapp/utils/Constants.dart';
import 'package:retrivalapp/utils/Utility.dart';
import 'package:retrivalapp/utils/appKeys.dart';
import 'package:toast/toast.dart';

import 'PrimaryCocoPresenter.dart';
import 'PrimaryCocoResonse.dart';

class PrimaryCocoListActivity extends StatefulWidget {
  PrimaryCocoListActivity({ Key key }) : super(key: key);
  @override
  State createState() => PrimaryCocoScreen();
}

class PrimaryCocoScreen extends State<PrimaryCocoListActivity> implements PrimaryCocoView {
  final String TAG = "PrimaryCocoListActivity";
  PrimaryCocoPresenter presenter;
  List<Datum> dataList;

  Widget appBarTitle = new Text("Select Store", style: new TextStyle(color: Colors.white),);
  Icon actionIcon = new Icon(Icons.search, color: Colors.white,);
  final key = new GlobalKey<ScaffoldState>();
  final TextEditingController searchQuery = new TextEditingController();
  bool isSearching;
  String searchText = "";


  searchListState() {
    searchQuery.addListener(() {
      if (searchQuery.text.isEmpty) {
        setState(() {
          isSearching = false;
          searchText = "";
        });
      }
      else {
        setState(() {
          isSearching = true;
          searchText = searchQuery.text;
        });
      }
    });
  }

  @override
  void initState() {
    super.initState();
    presenter = PrimaryCocoPresenter(this);
    presenter.getStoreList();
    isSearching = false;
    searchListState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: key,
      appBar: buildBar(context),
      body: dataList == null? Center(child: Utility.buildProgressIndicator(true),): ListView(
        padding: new EdgeInsets.symmetric(vertical: 8.0),
        children: isSearching ? _buildSearchList() : _buildList(),
      ),
    );
  }

  List<ChildItem> _buildList() {
    return dataList.map((Datum) => new ChildItem(Datum)).toList();
  }

  List<ChildItem> _buildSearchList() {
    if (searchText.isEmpty) {
      return dataList.map((Datum) => new ChildItem(Datum)).toList();
    }
    else {
      List<Datum> _searchList = List();
      for (int i = 0; i < dataList.length; i++) {
        String  name = dataList[i].storeName;
        if (name.toLowerCase().contains(searchText.toLowerCase())) {
          _searchList.add(dataList[i]);
        }
      }
      return _searchList.map((contact) => new ChildItem(contact))
          .toList();
    }
  }

  Widget buildBar(BuildContext context) {
    return new AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: colorPrimary,
        title: appBarTitle,
        actions: <Widget>[
          new IconButton(icon: actionIcon, onPressed: () {
            setState(() {
              if (this.actionIcon.icon == Icons.search) {
                this.actionIcon = new Icon(Icons.close, color: Colors.white,);
                this.appBarTitle = new TextField(
                  controller: searchQuery,
                  style: new TextStyle(
                    color: Colors.white,

                  ),
                  decoration: new InputDecoration(
                      prefixIcon: new Icon(Icons.search, color: Colors.white),
                      hintText: "Search Store...",
                      hintStyle: new TextStyle(color: Colors.white)
                  ),
                );
                _handleSearchStart();
              }
              else {
                _handleSearchEnd();
              }
            });
          },),
        ]
    );

  }

  void _handleSearchStart() {
    setState(() {
      isSearching = true;
    });
  }

  void _handleSearchEnd() {
    setState(() {
      this.actionIcon = new Icon(Icons.search, color: Colors.white,);
      this.appBarTitle =
      new Text("Select Store", style: new TextStyle(color: Colors.white),);
      isSearching = false;
      searchQuery.clear();
    });
  }




  @override
  void onFailed(String errorMessage) {
    Toast.show(errorMessage, context,
        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
  }

  @override
  void populateStoreList(List<Datum> data) {
    setState(() {
      this.dataList = data;
    });
  }
}

class ChildItem extends StatelessWidget {
  TextStyle style = TextStyle(color: Colors.grey, fontFamily: 'Montserrat', fontSize: 18.0);
  final Datum _datum;
  ChildItem(this._datum);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        onStoreSelected(context);
      },
      child: Card(
        child: Container(
          width: screenWidth(context),
          height: 38,
          margin: const EdgeInsets.only(left: 12,top: 28.0),
          child: Text(
            _datum.storeName,
            maxLines: 2,
            style:style ,
          ),
        ),
      ),
    );

  }

  void onStoreSelected(BuildContext context){
    /**
     * Hide keyboard
     */
    FocusScope.of(context).requestFocus(new FocusNode());

    Utility.setStringPreference(Constants.PRIMARY_STORE_ID, _datum.storeId);
    Utility.setStringPreference(Constants.PRIMARY_STORE_CODE, _datum.storeCode);
    Utility.setStringPreference(Constants.PRIMARY_WMS_STORE_ID, _datum.wmsStoreId);
    Utility.setStringPreference(Constants.PRIMARY_STORE_NAME, _datum.storeName);
    Utility.setStringPreference(Constants.PRIMARY_LOCATION_ID, _datum.locationId);

    Toast.show(_datum.storeName, context,duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeActivity()));

  }

  bool equalsIgnoreCase(String string1, String string2) {
    return string1?.toLowerCase() == string2?.toLowerCase();
  }

}

// To parse this JSON data, do
//
//     final primaryCocoResponse = primaryCocoResponseFromMap(jsonString);

import 'dart:convert';

class PrimaryCocoResponse {
  PrimaryCocoResponse({
    this.status,
    this.message,
    this.isGovernmentOrderEnable,
    this.data,
  });

  bool status;
  String message;
  bool isGovernmentOrderEnable;
  List<Datum> data;

  factory PrimaryCocoResponse.fromJson(String str) => PrimaryCocoResponse.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory PrimaryCocoResponse.fromMap(Map<String, dynamic> json) => PrimaryCocoResponse(
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
    isGovernmentOrderEnable: json["isGovernmentOrderEnable"] == null ? null : json["isGovernmentOrderEnable"],
    data: json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromMap(x))),
  );

  Map<String, dynamic> toMap() => {
    "status": status == null ? null : status,
    "message": message == null ? null : message,
    "isGovernmentOrderEnable": isGovernmentOrderEnable == null ? null : isGovernmentOrderEnable,
    "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toMap())),
  };
}

class Datum {
  Datum({
    this.storeId,
    this.storeCode,
    this.storeName,
    this.wmsStoreId,
    this.locationId,
  });

  String storeId;
  String storeCode;
  String storeName;
  String wmsStoreId;
  String locationId;

  factory Datum.fromJson(String str) => Datum.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Datum.fromMap(Map<String, dynamic> json) => Datum(
    storeId: json["store_id"] == null ? null : json["store_id"],
    storeCode: json["store_code"] == null ? null : json["store_code"],
    storeName: json["store_name"] == null ? null : json["store_name"],
    wmsStoreId: json["wms_store_id"] == null ? null : json["wms_store_id"],
    locationId: json["location_id"] == null ? null : json["location_id"],
  );

  Map<String, dynamic> toMap() => {
    "store_id": storeId == null ? null : storeId,
    "store_code": storeCode == null ? null : storeCode,
    "store_name": storeName == null ? null : storeName,
    "wms_store_id": wmsStoreId == null ? null : wmsStoreId,
    "location_id": locationId == null ? null : locationId,
  };
}

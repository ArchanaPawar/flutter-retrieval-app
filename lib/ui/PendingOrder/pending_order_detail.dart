import 'package:flutter/material.dart';
import 'package:retrivalapp/utils/AppColor.dart';

class PendingOrderDetail extends StatefulWidget {
  String Title ;

  PendingOrderDetail(this.Title, this.Data);

  var Data;
  @override
  _PendingOrderDetailState createState() => _PendingOrderDetailState();
}

class _PendingOrderDetailState extends State<PendingOrderDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(title: Text(widget.Title),
        backgroundColor: colorPrimary,),
      body: ListView.builder(
          shrinkWrap: true,
          itemCount: widget.Data.length,
          itemBuilder: (BuildContext context, int index) {
            return new Card(
              elevation: 4.0,
              child: new Container(
                padding: EdgeInsets.all(10.0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Order Id : "+data[index]["order_id"].toString()),
                    SizedBox(height: 5.0,),
                    Text("Short Quantity : "+data[index]["srt_quantity"].toString()),
                    SizedBox(height: 5.0,),
                    Text("Short Status : "+getStatus(data[index]["srt_status"].toString())),
                  ],
                ),
              ),
            );
          }),
    );
  }

  List data=new List();
  @override
  void initState() {
    print(widget.Data);
    for(int i=0;i<widget.Data.length;i++)
      {
        data.add(widget.Data[i]);
      }
    setState(() {

    });
  }

  String getStatus(String string) {
    switch(string)
    {
      case "1":
        return "Short Quantity";
      case "2":
        return "Not Found";
      default:
        return string;
    }
  }
}
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:intl/intl.dart';
import 'package:retrivalapp/ui/PendingOrder/pending_order_detail.dart';
import 'package:retrivalapp/ui/home/RetrievalMenuResponse.dart';
import 'package:retrivalapp/ui/order_details/OrderDetailsNew/GlobalData.dart';
import 'package:retrivalapp/ui/order_details/OrderDetailsNew/OrderDeatilHome.dart';
import 'package:retrivalapp/ui/order_details/database/DatabaseHelper.dart';
import 'package:retrivalapp/ui/order_list/OrderListModel.dart';
import 'package:retrivalapp/ui/order_list/OrderListPresenter.dart';
import 'package:retrivalapp/ui/order_list/OrderListView.dart';
import 'package:retrivalapp/ui/splash/SplashActivity.dart';
import 'package:retrivalapp/utils/AppColor.dart';
import 'package:retrivalapp/utils/Constants.dart';
import 'package:retrivalapp/utils/Dialogs.dart';
import 'package:retrivalapp/utils/Utility.dart';
import 'package:toast/toast.dart';
import 'package:retrivalapp/utils/APIConstant.dart';
import 'package:retrivalapp/utils/ApiController.dart';
import 'package:retrivalapp/utils/AppColor.dart';
import 'package:retrivalapp/utils/Constants.dart';
import 'package:retrivalapp/utils/Dialogs.dart';
import 'package:retrivalapp/utils/Message.dart';
import 'package:retrivalapp/utils/NetworkCheck.dart';
import 'package:retrivalapp/utils/Utility.dart';
import 'package:toast/toast.dart';

class StoreStockActivity extends StatefulWidget {
  @override
  OrderListScreen createState() => OrderListScreen();
}

class OrderListScreen extends State<StoreStockActivity> with SingleTickerProviderStateMixin
{
  final String TAG = "StoreStockActivity";
  OrderListPresenter presenter;
  TabController controller;
  int currentTab = 0;
  Text selectedStoreName;
  String selectedDate = "";
  Widget appBarTitle;
  DateTime selectedDateTime = DateTime.now();
  DateTime todayDate = DateTime.now();
  final TextEditingController _searchController = new TextEditingController();
  List<DataModel> pendingDataList = new List();
  List<DataModel> retrievedDataList = new List();
  var global_key = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return new DefaultTabController(
      length: 2,
      child: new Scaffold(
        key: global_key,
        appBar: new AppBar(
          backgroundColor: colorPrimary,
          actions: [

            InkWell(
              child: Icon(
                Icons.date_range,
                color: Colors.white,
              ),

              onTap: () {
                selectDate(context);
              },
            ),

            SizedBox(width: 10.0,),
            InkWell(
              child: Icon(
                Icons.history,
                color: Colors.white,
              ),

              onTap: () {
                onRefreshClick();
              },
            ),
            SizedBox(width: 10.0,),

          ],
          title: appBarTitle,
          bottom: new TabBar(
            controller: controller,
            onTap: handleTabSelection,
            tabs: [
              Tab(
                text: "Pending Stock",
              ),
              Tab(
               text: "Procured Stock",
              ),
            ],
          ),
        ),

        body: new TabBarView(
          controller: controller,
          children: <Widget>[
            pendingDataList.length!=0? _pendingOrderList():new Container(),
           retrievedDataList.length!=0? _retrievedOrderList():new Container(),
          ],
        ),
      ),
    );
  }
  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    controller = TabController(
      length: 2,
      vsync: this,
    );
    controller.addListener(handleTabViewSelection);

    super.initState();

    if (selectedDate.isEmpty) {
      var now = new DateTime.now();
      var formatter = new DateFormat('dd-MM-yyyy');
      selectedDate = formatter.format(now);
      var apiFormat = new DateFormat('yyyy-MM-dd');
      Utility.setStringPreference(Constants.ORDER_DATE, apiFormat.format(now));
    }
    setAppBarTitle(context);
    //loadOrderList(currentTab,"118");
    LoadProducts();
  }


  void LoadProducts() async {
    pendingDataList=new List();
    retrievedDataList=new List();
    setState(() {

    });
    ApiController apiController = new ApiController.internal();
    if (await NetworkCheck.check()) {
      Dialogs.showProgressDialog(await context);
      String storeId = (await Utility.getStringPreference(Constants.STORE_CODE));
      String userId =  (await Utility.getStringPreference(Constants.USER_ID));
      String url = APIConstant.getProductInOrder_Pending + selectedDate;
      apiController.getsNew(url).then((apiResponse) async
      {
        var data = apiResponse;
        var data1 = json.decode(data.body);
        Dialogs.hideProgressDialog(context);
        //await DatabaseHelper.db.clearDatabase();
        if (data1['status'] == true) {
          var products = data1['data'];
          for(int i=0;i<products.length;i++)
          {
            if(products[i]["procure_status"].toString()!="2")
              {
                pendingDataList.add(new DataModel(products[i]));

              }else
                {
                  retrievedDataList.add(new DataModel(products[i]));
                }
          }
          setState(()
          {
          });
        } else if (data1['status'] == false && data1['message'].toString().toLowerCase().contains("token"))
        {

          onFailed("No Product Found");
        }
      }).catchError((error) {
        Utility.log(TAG, error.toString());
        onFailed(Message.SOMETHING_WENT_WRONG);
      });
    } else
      {
      onFailed(Message.NO_INTERNET);
    }
  }


  void LoadAlertData(String Id) async {
    ApiController apiController = new ApiController.internal();
    if (await NetworkCheck.check()) {
      Dialogs.showProgressDialog(await context);
      String url = APIConstant.getProductInOrder_PendingStockView + Id;
      apiController.getsNew(url).then((apiResponse) async
      {
        var data = apiResponse;
        var data1 = json.decode(data.body);
        Dialogs.hideProgressDialog(context);
        //await DatabaseHelper.db.clearDatabase();
        if (data1['status'] == true) {
          var products = data1['data'];
          Alertdata=new List();
          for(int i=0;i<products.length;i++)
          {
            Alertdata.add(new DataModel(products[i]));
          }
          createDialog();
        } else if (data1['status'] == false && data1['message'].toString().toLowerCase().contains("token"))
        {
        }
      }).catchError((error) {
        Utility.log(TAG, error.toString());
        onFailed(Message.SOMETHING_WENT_WRONG);
      });
    } else
      {
      onFailed(Message.NO_INTERNET);
    }
  }
  String AlertTitle="";
  List<DataModel> Alertdata=new List();

  void setAppBarTitle(BuildContext context) async {
    String storeName =
    await Utility.getStringPreference(Constants.PRIMARY_STORE_NAME);
    if (storeName.isEmpty) {
      storeName = await Utility.getStringPreference(Constants.STORE_NAME);
    }

    selectedStoreName = new Text(
      "Store Stock",
      style: new TextStyle(color: Colors.white, fontSize: 20),
    );

    appBarTitle = GestureDetector(
      onTap: () {},
      child: new Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Center(child: Container(child: selectedStoreName)),
            Padding(
                padding: EdgeInsets.fromLTRB(0, 5, 0, 0),
                child: Text("(" + selectedDate + ")",
                    style: new TextStyle(color: Colors.white, fontSize: 15))),
          ]),
    );
    setState(() {});
  }

  void updateSelectedDate(String date) {
    setState(() {
      selectedDate = date;
      setAppBarTitle(context);
    });
  }

  void handleTabSelection(int tabIndex) {
    setState(() {
      currentTab = controller.index;
    });
    if(currentTab==1)
      {
        LoadProducts();
      }
  }

  void handleTabViewSelection() {
    setState(() {
      currentTab = controller.index;
    });
  }
  Future<Null> selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDateTime,
        firstDate: todayDate.subtract(new Duration(days: 4)),
        lastDate: todayDate.add(new Duration(days: 2)));
    if (picked != null && picked != selectedDate)
      setState(() {

        selectedDateTime = picked;
        var apiFormat = new DateFormat('yyyy-MM-dd');
        Utility.setStringPreference(
            Constants.ORDER_DATE, apiFormat.format(picked));

        var formatter = new DateFormat('dd-MM-yyyy');
        String ddmmyyyy = formatter.format(picked);

        updateSelectedDate(ddmmyyyy);
        LoadProducts();
        //loadOrderList(currentTab, "215");
      });
  }

  void loadOrderList(int currentTab, String linenumber) async {
    Dialogs.showProgressDialog(await context);
//    print("=================================================>Line number: "+linenumber);
    presenter.getOrderList(currentTab);
  }
   SlidableController slidableController ;
  void handleSlideIsOpenChanged(bool isOpen) {
    setState(() {
    //  _fabColor = isOpen ? Colors.green : Colors.blue;
    });
  }
  // Pending Order List
  Widget _pendingOrderList() {


    slidableController=new SlidableController(
     onSlideIsOpenChanged: (val)
        {
          print("valdata $val");
        },

     onSlideAnimationChanged: (val) {
        print("val changed $val");
      }
    );

    return ListView.builder(
        shrinkWrap: true,
        itemCount: pendingDataList.length,
        itemBuilder: (BuildContext context, int index) {
          return new GestureDetector(
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => PendingOrderDetail(pendingDataList[index].data["name"],pendingDataList[index].data["order_details"])));
              },
            child:
            Slidable(
            controller: slidableController,
            actionPane: SlidableDrawerActionPane(),
            actionExtentRatio: 0.25,
                actions: <Widget>[
                  IconSlideAction(
                    caption: '',
                    icon: Icons.check,
                    color: Colors.green,
                    onTap: () {
                      updateProcure(index);
                      print(index);
                    },
                  ),
                  IconSlideAction(
                    caption: '',
                    icon: Icons.close,
                    color: Colors.grey,
                    onTap: () {
                      slidableController.activeState?.open();
                      Slidable.of(context)?.close();
                    },
                  ),
                ],
                child: Card(
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children:
                [
                  new Row(
                    children: [
                      Expanded(flex: 2,
                      child: Container(
                        alignment: Alignment.centerLeft,
                        width: 100.0,
                        height: 100.0,
                        child: Image.network(
                          pendingDataList[index].data["image_url"],
                          fit: BoxFit.cover,
                        ),
                      ),),
                      SizedBox(width: 20.0,),
                      Expanded(flex: 8,
                      child:new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 10.0,),
                         new Row(
                           children: [
                             Expanded(child:   Text("PRO ID : "+pendingDataList[index].data["product_id"]),),
                             Expanded(child:   Text("SRT QTY : "+pendingDataList[index].data["srt_quantity"],textAlign: TextAlign.right),),
                           ],
                         ),

                          SizedBox(height: 5.0,),
                          Text(pendingDataList[index].data["name"]),

                          SizedBox(height: 5.0,),
                          new Row(
                            children: [
                              Expanded(child:   Text("WEIGHT : "+pendingDataList[index].data["weight"].toString()),),
                              Expanded(child:   Text("MRP: "+pendingDataList[index].data["mrp"].toString(),textAlign: TextAlign.right,),),
                            ],
                          ),
                          SizedBox(height: 5.0,),
                          new Row(
                            children: [
                              Expanded(child:   Text("Procured QTY : "+pendingDataList[index].data["procure_quantity"].toString(),style: TextStyle(color: Colors.green),),),
                            ],
                          ),
                          SizedBox(height: 10.0,),
                          new Container(
                            width: MediaQuery.of(context).size.width,
                            child: InkWell(
                              onTap: (){
                                LoadAlertData(pendingDataList[index].data["product_id"]);
                                AlertTitle=pendingDataList[index].data["name"];
                              },
                              child: Text("View Stock",style: TextStyle(color: Colors.blue,fontSize: 22),textAlign: TextAlign.center,),
                            ),
                          ),
                          SizedBox(height: 10.0,),
                        ],
                      )),
                      SizedBox(width: 20.0,),
                    ],
                  ),

                ],
              ),
            )),
          );
        });
  }

  Widget _retrievedOrderList() {
    return  retrievedDataList.length!=0?ListView.builder(
        shrinkWrap: true,
        itemCount: retrievedDataList.length,
        itemBuilder: (BuildContext context, int index) {
          return new GestureDetector(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => PendingOrderDetail(retrievedDataList[index].data["name"],retrievedDataList[index].data["order_details"])));
              // onRowClick(pendingDataList[index]);
            },
            child: Card(
              color: Colors.white,
              child: new Container(
                decoration: new BoxDecoration(
                  gradient: new LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: <Color>[
                      Colors.pink[100],
                      Colors.white
                    ],),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    new Row(
                      children: [
                        Expanded(flex: 2,
                          child: Container(
                            padding: EdgeInsets.all(5.0),
                            width: 100.0,
                            height: 100.0,
                            child: FadeInImage(
                              // here `bytes` is a Uint8List containing the bytes for the in-memory image
                              placeholder: AssetImage("drawable/app_logo_circle_transprent.png"),
                              image: NetworkImage( retrievedDataList[index].data["image_url"].toString()),
                            )/*Image.network(
                              retrievedDataList[index].data["image_url"],
                              fit: BoxFit.fill,
                            ),*/
                          ),),
                        SizedBox(width: 20.0,),
                        Expanded(flex: 8,
                            child:new Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                new Row(
                                  children:
                                  [
                                    Expanded(child:   Text("PRO ID : "+retrievedDataList[index].data["product_id"]),),
                                    Expanded(child:   Text("SRT QTY : "+retrievedDataList[index].data["srt_quantity"],textAlign: TextAlign.right),),
                                  ],
                                ),
                                SizedBox(height: 5.0,),
                                Text(retrievedDataList[index].data["name"]),

                                SizedBox(height: 5.0,),
                                new Row(
                                  children: [
                                    Expanded(child:   Text("WEIGHT : "+retrievedDataList[index].data["weight"].toString()),),
                                    Expanded(child:   Text("MRP: "+retrievedDataList[index].data["mrp"].toString(),textAlign: TextAlign.right,),),
                                  ],
                                ),
                                SizedBox(height: 5.0,),


                              ],
                            )),
                        SizedBox(width: 20.0,),
                      ],
                    ),

                  ],
                ),
              ),
            ),
          );
        }) :new Container();
  }

  void onRowClick(OrderListModel model) {
    if (currentTab == 0) {
      Utility.setStringPreference(Constants.ORDER_ID, model.orderId);
      if (model.lockStatus == 0) {
        Utility.setBooleanPreference(Constants.IS_ORDER_LOCK, false);
        /**
         * delete product from retrieval product if exist
         */
      } else {
        Utility.setBooleanPreference(Constants.IS_ORDER_LOCK, true);
      }

      /**
       * Clear both product table
       */
      String delivery_date = model.deliveryDate.toString();
      String delivery_time = model.deliveryTime.toString();
      String Lock = model.lockStatus.toString();
      Navigator.push(context, MaterialPageRoute(
          builder: (context) => OrderDetailHome(delivery_date, delivery_time)));
    }
  }

  void onRefreshClick() {
    //loadOrderList(currentTab, "917");

    var now = new DateTime.now();
    var formatter = new DateFormat('dd-MM-yyyy');
    selectedDate = formatter.format(now);
    var apiFormat = new DateFormat('yyyy-MM-dd');
    Utility.setStringPreference(Constants.ORDER_DATE, apiFormat.format(now));
    setAppBarTitle(context);
    //loadOrderList(currentTab,"118");
    LoadProducts();
  }

  String setDeliveryDateAndTine(OrderListModel model) {

    /**
     * get month
     */
    String month = model.deliveryDate.month.toString();
    if (month.length < 2) {
      month = "0" + month;
    }


    /**
     * get date
     */

    String day = model.deliveryDate.day.toString();
    if (day.length < 2) {
      day = "0" + day;
    }

    return (day + "-" + month + "\n" + model.deliveryTime);
  }

  @override
  void onFailed(String errorMessage) {
    Dialogs.hideProgressDialog(context);
    Toast.show(
      errorMessage, context, duration: Toast.LENGTH_LONG, gravity: Toast.TOP,);
  }

  void createDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context)
        {
          return AlertDialog(
            actionsPadding: EdgeInsets.all(0.0),
            contentPadding: EdgeInsets.all(0.0),
            content: Container(
              width: MediaQuery.of(context).size.width,
              child: Column(
                  children: <Widget>[
                    Expanded(flex: 1,
                    child: new Container(
                      color: colorPrimary,
                      child: new Row(
                        children: [
                          Expanded(flex: 9, child: Text(AlertTitle,style: TextStyle(fontSize: 20.0,color: Colors.white),textAlign: TextAlign.center,),),
                          Expanded(
                            flex: 1, child:InkWell(
                              onTap: (){
                                Navigator.of(context).pop();
                              },
                              child:  Icon(Icons.clear,size: 30.0,color: Colors.white,),
                            ),
                          )
                        ],
                      ),
                    ),),
                    Expanded(
                      flex: 9,
                        child: ListView.builder(
                            shrinkWrap: true,
                            itemCount: Alertdata.length,
                            itemBuilder: (BuildContext context, int index) {
                              return new Card(
                                elevation: 4.0,
                                child: new Container(
                                  padding: EdgeInsets.all(10.0),
                                  child: new Row(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        child: Text(Alertdata[index].data["store_name"]),
                                      ),
                                      Expanded(
                                        child: Text(Alertdata[index].data["stock"]+"  "+Alertdata[index].data["stock_unit"],textAlign: TextAlign.right,),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            })
                    )
                  ]
              ),
            ),
          );
        }
    );
  }

  Future<void> updateProcure(int index) async {

    String xml_data = "";
    for (int i = 0; i < pendingDataList[index].data["order_details"].length; i++) {
      var data=pendingDataList[index].data["order_details"][i];
      xml_data = xml_data +
          "<products>\n " +
          "<order_id>" + data["order_id"] + "</order_id>\n " +
          "<product_id>" + pendingDataList[index].data["product_id"] + "</product_id>\n" +
          "</products>\n ";

    }

  String  orderId = (await Utility.getStringPreference(Constants.ORDER_ID));
    String storeId = (await Utility.getStringPreference(Constants.STORE_CODE));
    String userId = (await Utility.getStringPreference(Constants.USER_ID));
    String EMPLOYEE_ID = (await Utility.getStringPreference(Constants.EMPLOYEE_ID));

    ApiController apiController = new ApiController.internal();

    var xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
        "<preCureData>\n " +
        "<coco>" +
        await Utility.getStringPreference(Constants.STORE_ID) +
        "</coco>\n " +
        xml_data +
        "<employee_id>" +
        await Utility.getStringPreference(Constants.EMPLOYEE_ID) +
        "</employee_id>\n " +
        "<user_id>" +
        userId +
        "</user_id>\n " +
        "</preCureData>";

    Utility.log(TAG, "Response: " + xml);
    apiController
        .postXmlDataWithHeader(APIConstant.updateProcureStatus, xml)
        .then((apiResponse) async {
      var data = apiResponse;
      Utility.log(TAG, "Response: " + data);
      RetrievalMenuResponse response = RetrievalMenuResponse.fromJson(apiResponse);
        if(response.status==true)
        {
          pendingDataList.removeAt(index);
          setState(() {
          });
        }else
          {
            GlobalWidget.showToast(context, response.message);
          }


    });
  }
}


class DataModel
{
  var data;

  DataModel(this.data);
}

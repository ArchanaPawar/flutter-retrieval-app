// To parse this JSON data, do
//
//     final loginResponse = loginResponseFromMap(jsonString);

import 'dart:convert';

class LoginResponse {
  LoginResponse({
    this.status,
    this.message,
    this.token,
    this.data,
  });

  bool status;
  String message;
  String token;
  Data data;

  factory LoginResponse.fromJson(String str) =>
      LoginResponse.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory LoginResponse.fromMap(Map<String, dynamic> json) => LoginResponse(
        status: json["status"] == null ? null : json["status"],
        message: json["message"] == null ? null : json["message"],
        token: json["token"] == null ? null : json["token"],
        data: json["data"] == null ? null : Data.fromMap(json["data"]),
      );

  Map<String, dynamic> toMap() => {
        "status": status == null ? null : status,
        "message": message == null ? null : message,
        "token": token == null ? null : token,
        "data": data == null ? null : data.toMap(),
      };
}

class Data {
  Data({
    this.id,
    this.employeeId,
    this.name,
    this.telephone,
  });

  String id;
  String employeeId;
  String name;
  String telephone;

  factory Data.fromJson(String str) => Data.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Data.fromMap(Map<String, dynamic> json) => Data(
        id: json["id"] == null ? null : json["id"],
        employeeId: json["employee_id"] == null ? null : json["employee_id"],
        name: json["name"] == null ? null : json["name"],
        telephone: json["telephone"] == null ? null : json["telephone"],
      );

  Map<String, dynamic> toMap() =>
      {
        "id": id == null ? null : id,
        "employee_id": employeeId == null ? null : employeeId,
        "name": name == null ? null : name,
        "telephone": telephone == null ? null : telephone,
      };
}

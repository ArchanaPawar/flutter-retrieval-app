
import 'package:retrivalapp/ui/login/LoginResponse.dart';

abstract class LoginView {
  void onFailed(String errorMessage);
  void onSelectStore();
  void onLoginSuccess(Data data);
  String getUserName();
  String getPassword();
}

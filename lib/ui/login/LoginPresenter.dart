import 'package:retrivalapp/ui/login/LoginResponse.dart';
import 'package:retrivalapp/ui/login/LoginView.dart';
import 'package:retrivalapp/utils/APIConstant.dart';
import 'package:retrivalapp/utils/ApiController.dart';
import 'package:retrivalapp/utils/Constants.dart';
import 'package:retrivalapp/utils/Message.dart';
import 'package:retrivalapp/utils/NetworkCheck.dart';
import 'package:retrivalapp/utils/Utility.dart';

class LoginPresenter {
  final String TAG = "LoginPresenter";
  LoginView view;
  LoginPresenter(this.view);
  ApiController apiController = new ApiController.internal();

  void login() async {
    if (await NetworkCheck.check()) {
      var xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
          "<user_info>\n " +
          "<emp_id>" + view.getUserName() + "</emp_id>\n" +
          "<app_type>retrievalapp</app_type>\n" +
          "<password>" + view.getPassword() + "</password>\n" +
          "</user_info>";
      apiController.postXmlDataWithHeader(APIConstant.login, xml).then((String apiResponse) {
        Utility.log(TAG, "Response: " + apiResponse);

        LoginResponse response = LoginResponse.fromJson(apiResponse);
        if (response.status) {
          Utility.setStringPreference(Constants.ACCESS_TOKEN, response.token);
          view.onLoginSuccess(response.data);
        } else {
          view.onFailed(response.message);
        }
      }).catchError((error) {
        Utility.log(TAG, error.toString());
        view.onFailed(Message.SOMETHING_WENT_WRONG);
      });
    } else {
      view.onFailed(Message.NO_INTERNET);
    }
  }
}

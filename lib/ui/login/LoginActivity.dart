import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flutter/material.dart';
import 'package:retrivalapp/ui/login/LoginPresenter.dart';
import 'package:retrivalapp/ui/login/LoginResponse.dart';
import 'package:retrivalapp/ui/login/LoginView.dart';
import 'package:retrivalapp/ui/order_details/database/DatabaseHelper.dart';
import 'package:retrivalapp/ui/select_store/SelectStoreActivity.dart';
import 'package:retrivalapp/utils/AppColor.dart';
import 'package:retrivalapp/utils/Constants.dart';
import 'package:retrivalapp/utils/Dialogs.dart';
import 'package:retrivalapp/utils/Message.dart';
import 'package:retrivalapp/utils/Resources.dart';
import 'package:retrivalapp/utils/Utility.dart';
import 'package:retrivalapp/utils/appKeys.dart';
import 'package:toast/toast.dart';

class LoginActivity extends StatefulWidget {
  @override
  State createState() => LoginScreen();
}

class LoginScreen extends State<LoginActivity> implements LoginView {
  final String TAG = "LoginActivity";
  TextStyle style = TextStyle(color: Colors.white, fontFamily: 'Montserrat', fontSize: 20.0);
  LoginPresenter presenter;
  final TextEditingController _userNameController = new TextEditingController();
  final TextEditingController _passwordController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    presenter = LoginPresenter(this);
  }

  @override
  Widget build(BuildContext context) {
    final userNameField = TextFormField(
      obscureText: false,
      style: style,
      keyboardType: TextInputType.number,
      maxLength: 15,

      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "User Name",
          hintStyle: TextStyle(fontSize: 20.0, color: Colors.grey),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.grey),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          counter: Container(),
          prefixIcon: Image(
            image: AssetImage(Resources.user),
            width: 24,
            height: 24,
          )),
      controller: _userNameController,
    );

    final passwordField = TextFormField(
      obscureText: true,
      style: style,
      maxLength: 15,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Password",
          hintStyle: TextStyle(fontSize: 20.0, color: Colors.grey),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.grey),
          ),
          counter: Container(),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          prefixIcon: Image(
            image: AssetImage(Resources.lock),
            width: 24,
            height: 24,
          )),
      controller: _passwordController,
    );

    final loginButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Colors.white,
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          onLoginButtonCLick();
        },
        child: Text("Login",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: colorPrimaryDark, fontWeight: FontWeight.normal)),
      ),
    );

    return Scaffold(
      body: DoubleBackToCloseApp(
        child:  Scaffold(
            body: Container(
              width: screenWidth(context),
              height: screenHeight(context),
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage(Resources.splash_bg), fit: BoxFit.fill)),
              child: SingleChildScrollView(
                child: Center(
                  child: Container(
                    color: Colors.transparent,
                    child: Padding(
                      padding: const EdgeInsets.all(36.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                            height: 25,
                          ),
                          Image(
                            image: AssetImage(Resources.splash_logo),
                            width: 150,
                            height: 150,
                          ),
                          SizedBox(
                            height: 0,
                            child: Image.asset(Resources.splash_logo,
                              fit: BoxFit.contain,
                            ),
                          ),
                          SizedBox(height: 45.0),
                          userNameField,
                          SizedBox(height: 25.0),
                          passwordField,
                          SizedBox(
                            height: 35.0,
                          ),
                          loginButton,
                          SizedBox(
                            height: 15.0,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            )),
        snackBar: const SnackBar(
          content: Text('Tap back again to leave'),
        ),
      ),
    );

  }

  void onLoginButtonCLick() {
    /**
     * Hide keyboard
     */
    FocusScope.of(context).requestFocus(new FocusNode());

    if (getUserName().isEmpty) {
      Dialogs.ackAlert(context, Message.emptyUserName);

    } else if (getPassword().isEmpty) {
      Dialogs.ackAlert(context, Message.emptyPassword);

    } else {
      presenter.login();
    }
  }

  @override
  String getPassword() {
    return _passwordController.text.trim();
  }

  @override
  String getUserName() {
    return _userNameController.text.trim();
  }

  @override
  void onFailed(String errorMessage) {
    Toast.show(errorMessage, context,
        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
  }

  @override
  void onSelectStore() {
    /**
     * Clear database
     */
    try{
      DatabaseHelper.db.clearDatabase();
    } catch(error)
    {
      throw Exception(TAG+'=====>Database=====> ' + error.toString());
    }
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => SelectStoreActivity()));
  }

  @override
  void onLoginSuccess(Data data)
  {
    Utility.setStringPreference(Constants.USER_ID, data.id);
    Utility.setStringPreference(Constants.EMPLOYEE_ID, data.employeeId);
    Utility.setStringPreference(Constants.NAME, data.name);
    Utility.setBooleanPreference(Constants.IS_LOGIN, true);
    Toast.show("Welcome: " + data.name, context,duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    onSelectStore();
  }
}

import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:retrivalapp/ui/login/LoginActivity.dart';
import 'package:retrivalapp/ui/login/LoginResponse.dart';
import 'package:retrivalapp/ui/order_details/OrderDetailsNew/GlobalData.dart';
import 'package:retrivalapp/ui/order_details/database/DBConstant.dart';
import 'package:retrivalapp/ui/order_details/database/DatabaseHelper.dart';
import 'package:retrivalapp/ui/splash/SplashActivity.dart';
import 'package:retrivalapp/utils/APIConstant.dart';
import 'package:retrivalapp/utils/ApiController.dart';
import 'package:retrivalapp/utils/AppColor.dart';
import 'package:retrivalapp/utils/Constants.dart';
import 'package:retrivalapp/utils/Dialogs.dart';
import 'package:retrivalapp/utils/Message.dart';
import 'package:retrivalapp/utils/NetworkCheck.dart';
import 'package:retrivalapp/utils/Utility.dart';
import 'package:toast/toast.dart';
//Users/mac/AndroidStudioProjects/FlutterProject/retrival_app/lib/ui/order_details/OrderDetailsNew/OrderDeatilHome.dart
//Users/mac/AndroidStudioProjects/FlutterProject/retrival_app/lib/ui/order_details/OrderDetailsNew/OrderDeatilHome.dart
class OrderDetailHome extends StatefulWidget
{
  String delivery_date, delivery_time;
  String lock = "0";

  OrderDetailHome(this.delivery_date, this.delivery_time);

  @override
  State<StatefulWidget> createState() => MyorderView();
}

class MyorderView extends State<OrderDetailHome>
    with SingleTickerProviderStateMixin {
  int active = 1;
  int current_index = 0;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
        appBar: AppBar(
          title: Text('Product in order'),
          backgroundColor: colorPrimary,
        ),
        bottomNavigationBar: new Container(
          color: Colors.white,
          padding: EdgeInsets.only(top: 5.0),
          height: 60.0,
          child: new Row(
            children: [
              Expanded(
                child: new InkWell(
                  onTap: () {
                    setState(() {
                      active = 1;
                      getPending();
                    });
                  },
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      new Stack(children: <Widget>[
                      new Container(
                        margin: EdgeInsets.only(top: 5.0,right: 5.0),
                        child:   new Icon(
                          Icons.hourglass_empty,
                          size: GlobalWidget.getSize(),
                          color: active == 1
                              ? GlobalWidget.getcolor(true)
                              : GlobalWidget.getcolor(false),
                        ),
                      ),
                        new Positioned(
                            top: -0.1,
                            right: -0.1,
                            child: new Stack(
                              children: <Widget>[
                                new Container(
                                  alignment: Alignment.center,
                                  //padding: GlobalWidget.getpadding(),
                                  decoration: active == 1
                                      ? GlobalWidget.getDecoration(true)
                                      : GlobalWidget.getDecoration(false),
                                  constraints: GlobalWidget.getBoxConstraint(),
                                  child: new Text(
                                    '${pendingDataList.length.toString()}',
                                    style: GlobalWidget.getTextStyle(true),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ],
                            ))
                      ]),
                      Text(
                        "Pending",
                        style: active == 1
                            ? GlobalWidget.getTextStyleBottomText(true)
                            : GlobalWidget.getTextStyleBottomText(false),
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                child: new InkWell(
                  onTap: () {
                    active = 2;
                    setState(() {
                      getretrived();
                      /*
*/
                    });
                  },
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      new Stack(children: <Widget>[
                        new Container(
                          margin: EdgeInsets.only(top: 5.0,right: 5.0),
                          child: new Icon(
                            Icons.add_shopping_cart,
                            size: GlobalWidget.getSize(),
                            color: active == 2
                                ? GlobalWidget.getcolor(true)
                                : GlobalWidget.getcolor(false),
                          ),
                        ),
                        new Positioned(
                            top: -0.1,
                            right: -0.1,
                            child: new Stack(
                              children: <Widget>[
                                new Container(
                                  alignment: Alignment.center,
                                  padding: GlobalWidget.getpadding(),
                                  decoration: active == 2
                                      ? GlobalWidget.getDecoration(true)
                                      : GlobalWidget.getDecoration(false),
                                  constraints: GlobalWidget.getBoxConstraint(),
                                  child: new Text(
                                    '${retrivedDataList.length.toString()}',
                                    style: GlobalWidget.getTextStyle(true),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ],
                            ))
                      ]),
                      Text(
                        "Retrived",
                        style: active == 2
                            ? GlobalWidget.getTextStyleBottomText(true)
                            : GlobalWidget.getTextStyleBottomText(false),
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                child: new InkWell(
                  onTap: () {
                    setState(() {
                      active = 3;
                      getnotfound();
                    });
                  },
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      new Stack(children: <Widget>[
                       new Container(
                         margin: EdgeInsets.only(top: 5.0,right: 5.0),
                         child:  new Icon(
                           Icons.mood_bad,
                           size: GlobalWidget.getSize(),
                           color: active == 3
                               ? GlobalWidget.getcolor(true)
                               : GlobalWidget.getcolor(false),
                         ),
                       ),
                        new Positioned(
                            top: -0.1,
                            right: -0.1,
                            child: new Stack(
                              children: <Widget>[
                                new Container(
                                  alignment: Alignment.center,
                                  padding: GlobalWidget.getpadding(),
                                  decoration: active == 3
                                      ? GlobalWidget.getDecoration(true)
                                      : GlobalWidget.getDecoration(false),
                                  constraints: GlobalWidget.getBoxConstraint(),
                                  child: new Text(
                                    '${notFoundDataList.length.toString()}',
                                    style: GlobalWidget.getTextStyle(true),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ],
                            ))
                      ]),
                      Text(
                        "Not Found",
                        style: active == 3
                            ? GlobalWidget.getTextStyleBottomText(true)
                            : GlobalWidget.getTextStyleBottomText(false),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        body: new Column(
          children: [
            Expanded(
              flex: 1,
              child: new Container(
                color: Colors.grey,
                child: new Row(
                  children: [

                    Expanded(
                        child: new InkWell(
                          onTap: () {
                            getLockstatus();
                          },
                          child: new Container(
                            decoration: GlobalWidget.getHeadButtonShape(
                                widget.lock == "1" ? Colors.orange : Colors.green),
                            alignment: Alignment.center,
                            margin: GlobalWidget.getMargin(),
                            padding: GlobalWidget.getpaddingBtn(),
                            child: Text(widget.lock == "1" ? "Locked " : "Lock Order",
                                style: TextStyle(fontSize: 12, color: Colors.white)),
                          ),
                        )
                    ),
                    Expanded(

                      child: new Container(
                        margin: GlobalWidget.getMargin(),
                        alignment: Alignment.center,
                        child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(orderId,
                                style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold)),
                            Text("Item Count " + itemcount.toString(),
                                style: TextStyle(
                                    fontSize: 10,
                                    color: Colors.black,
                                    fontWeight: FontWeight.normal)
                                 )
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      child: InkWell(
                        onTap: () {
                          UpdateDone();
                        },
                        child: new Container(
                          decoration:
                          BoxDecoration(
                              color: const Color(0xFFd14d00),
                              borderRadius: BorderRadius.all(Radius.circular(10.0))),
                          alignment: Alignment.center,
                          margin: GlobalWidget.getMargin(),
                          padding: GlobalWidget.getpaddingBtn(),
                          child: Text("Done",
                              style:
                              TextStyle(fontSize: 12, color: Colors.white)),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Divider(
              color: Colors.grey,
              height: 2.0,
            ),
            Expanded(
              flex: 9,
              child: getOrderList(),
            )
          ],
        ));
  }

  String pending = "Pending";
  String retrived = "Retrived";
  String not_found = "NotFound";

  List pendingDataList = new List();
  List retrivedDataList = new List();
  List notFoundDataList = new List();

  // Pending Order List
  Widget _pendingOrderList() {
    return (pendingDataList.length != null && pendingDataList.length != 0)
        ? ListView.builder(
            shrinkWrap: true,
            itemCount: pendingDataList.length,
            itemBuilder: (BuildContext context, int index) {
              return new GestureDetector(
                onTap: () {
                  // onRowClick(pendingDataList[index]);
                },
                child: Card(
                  color: Colors.white,
                  child: Column(
                    children: [
                      getAboveRow(pendingDataList, index),
                      Container(
                        padding: EdgeInsets.all(10.0),
                        child: new Row(
                          children: [
                            Expanded(
                              flex: 5,
                              child: new RaisedButton(
                                  shape: getSahpe(0xFFFFA500),
                                  onPressed: () {
                                    String parent_product_id =
                                        GlobalWidget.getDatafromJson(
                                            pendingDataList[index]
                                                [DBConstant.PRODUCT_DATA],
                                            "parent_product_id");
                                    String offer_type =
                                        GlobalWidget.getDatafromJson(
                                            pendingDataList[index]
                                                [DBConstant.PRODUCT_DATA],
                                            "offer_type");
                                    if (widget.lock == "0") {
                                      GlobalWidget.showToast(context, Lockmsg);
                                    } else if (parent_product_id != "0")
                                    {
                                      current_index = index;
                                      retriveactionOfchild();
                                    } else {
                                      update(retrived, index,offer_type);
                                    }
                                  },
                                  color: const Color(0xFFd14d00),
                                  textColor: Colors.white,
                                  child: Text("OK",
                                      style: TextStyle(fontSize: 12))),
                            ),
                            Expanded(
                              flex: 1,
                              child: SizedBox(),
                            ),
                            Expanded(
                              flex: 5,
                              child: new RaisedButton(
                                  shape: getSahpe(0xFFFF0000),
                                  onPressed: ()
                                  {
                                    String val = GlobalWidget.getDatafromJson(pendingDataList[index][DBConstant.PRODUCT_DATA], "cart_offer_id");
                                    String parent_product_id = GlobalWidget.getDatafromJson(pendingDataList[index][DBConstant.PRODUCT_DATA], "parent_product_id");
                                    String nf_message = GlobalWidget.getDatafromJson(pendingDataList[index][DBConstant.PRODUCT_DATA], "nf_message");
                                    Utility.log(TAG, val);
                                    String offer_type = GlobalWidget.getDatafromJson(pendingDataList[index][DBConstant.PRODUCT_DATA], "offer_type");
                                    Utility.log(TAG, "offer_type : "+offer_type);
                                    Utility.log(TAG, parent_product_id);
                                    if (widget.lock == "0") {
                                      GlobalWidget.showToast(context, Lockmsg);
                                    } else if (val != "0")
                                    {
                                      GlobalWidget.showToast(context, "This is cart offer product . You can not remove only retrive . ");
                                    }else if(offer_type == "3"||offer_type=="4")
                                    {
                                      updatenotFound(not_found, index,offer_type);
                                    } else if (parent_product_id != "0")
                                      {
                                          Utility.log(TAG, parent_product_id+"  "+pendingDataList[index][DBConstant.PRODUCT_ID].toString());
                                          /* if(parent_product_id == pendingDataList[index][DBConstant.PRODUCT_ID].toString())
                                          {
                                            Utility.log(TAG, "update");
                                            update(not_found, index);
                                          }else
                                            {
                                              Utility.log(TAG, "not update");
                                              GlobalWidget.showToast(context, nf_message);
                                            }*/
                                          GlobalWidget.showToast(context, nf_message);
                                      }
                                      else {
                                      Utility.log(TAG, "not without parent");
                                      updatenotFound(not_found, index,offer_type);
                                    }
                                  },
                                  color: new Color(0xFFFF0000),
                                  textColor: Colors.white,
                                  child: Text("Not Found", style: TextStyle(fontSize: 12))),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );
            })
        : Center(child: Text("opps! no pending order"));
  }

  Widget _retrivedOrderList() {
    return (retrivedDataList.length != null && retrivedDataList.length != 0)
        ? ListView.builder(
            shrinkWrap: true,
            itemCount: retrivedDataList.length,
            itemBuilder: (BuildContext context, int index) {
              return new GestureDetector(
                onTap: () {
                  // onRowClick(pendingDataList[index]);
                },
                child: Card(
                  color: Colors.white,
                  child: Column(
                    children: [
                      getAboveRow(retrivedDataList, index),
                    ],
                  ),
                ),
              );
            })
        : Center(child: Text("opps! no retrived order"));
  }

  Widget _notFoundOrderList() {
    return (notFoundDataList.length != null && notFoundDataList.length != 0)
        ? ListView.builder(
            shrinkWrap: true,
            itemCount: notFoundDataList.length,
            itemBuilder: (BuildContext context, int index) {
              return new GestureDetector(
                onTap: () {
                  // onRowClick(pendingDataList[index]);
                },
                child: Card(
                  color: Colors.white,
                  child: Column(
                    children: [
                      getAboveRow(notFoundDataList, index),
                    ],
                  ),
                ),
              );
            })
        : Center(child: Text("opps! not found order not available"));
  }

  getSahpe(int color) {
    return RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
        side: BorderSide(color: new Color(color)));
  }

/*
  @override
  void init() {

    LoadProducts();
  }*/
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    LoadProducts();
  }

  String orderId = "";

  void LoadProducts() async
  {
    if (await NetworkCheck.check())
    {
      Dialogs.showProgressDialog(await context);
      orderId = (await Utility.getStringPreference(Constants.ORDER_ID));

      String storeId = (await Utility.getStringPreference(Constants.STORE_CODE));
      String userId = (await Utility.getStringPreference(Constants.USER_ID));
      String url = APIConstant.getProductInOrder +
          orderId +
          "/" +
          storeId +
          "/" +
          userId +
          "/" +
          storeId;
      apiController.getsNew(url).then((apiResponse) async {
        var data = apiResponse;
        var data1 = json.decode(data.body);

        Dialogs.hideProgressDialog(context);
        //await DatabaseHelper.db.clearDatabase();
        if (data1['status'] == true) {
          widget.lock = data1["order"]["lock_status"].toString();
          Utility.log("lockstatus", widget.lock);
          if (widget.lock == "0") {
            bool status = await DatabaseHelper.db.getLockedStatus(orderId);
            if (status == true) {
              widget.lock = "1";
            }
          }

         // onFailed("Done");
          var products = data1['products'];
          if (!await DatabaseHelper.db.checkOrderIfAlreadyExist("", orderId, "")) {
            for (int i = 0; i < products.length; i++) {
              Utility.log(
                  "cart_offer_id",
                  products[i].toString() + "\n" + products[i]['cart_offer_id'].toString());
              Map<String, dynamic> map() => {
                    'product_id': products[i]['product_id'].toString(),
                    'cart_offer_id': products[i]['cart_offer_id'].toString(),
                    'parent_product_id': products[i]['parent_product_id'].toString(),
                    'c_offer_id': products[i]['c_offer_id'].toString(),
                    'is_offer': products[i]['is_offer'].toString(),
                    'offer_type': products[i]['offer_type'].toString(),
                    'parent_product_name': products[i]['parent_product_name'].toString(),
                    'srt_message': products[i]['srt_message'].toString(),
                    'nf_message': products[i]['nf_message'].toString(),
                    'retrieve_message': products[i]['retrieve_message'].toString(),
                    'cart_offer_id': products[i]['cart_offer_id'].toString(),
                    'get_qty': products[i]['get_qty'].toString(),
                  };
              String result = jsonEncode(map());
              var products_data = products[i];
              int id = await DatabaseHelper.db.addProductInAllTable(
                products_data['product_id'].toString(),
                orderId.toString(),
                "" + products_data['name'].toString(),
                products_data['ord_qty'].toString(),
                products_data['parent_product_id'].toString(),
                products_data['image'].toString(),
                products_data['barcode'].toString(),
                "${products_data['ord_qty'].toString()}",
                "${products_data['total'].toString()}",
                "${products_data['stock_qty'].toString()}",
                "${result}",
              );
            }
          }
        } else if (data1['status'] == false &&
            data1['message'].toString().toLowerCase().contains("token")) {
          Utility.clearSharedPreference(context);
          Navigator.of(context)
              .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context) => SplashActivity()));
        }
        getPending();
        getretrived();
        getnotfound();

        getPendingCounter();

        /*  if (response.status) {
          Utility.log(TAG, "Responseval: " + response.order.toJson());
          populateOrderList(response, orderId);
        } else {
          onFailed(response.message);
        }*/
      }).catchError((error) {
        Utility.log(TAG, error.toString());
        onFailed(Message.SOMETHING_WENT_WRONG);
      });
    } else {
      getPending();
      getretrived();
      getnotfound();

      getPendingCounter();
      onFailed(Message.NO_INTERNET);
    }
  }

  @override
  void dispose() {
    //_countController.close();
    super.dispose();
  }

  void onFailed(String errorMessage) {
    Dialogs.hideProgressDialog(context);
    GlobalWidget.showToast(context, errorMessage);
  }

  void loadOrderList() async {
    Dialogs.showProgressDialog(await context);
    //  presenter.getProductInOrder();
  }

  final String TAG = "OrderDetailsPresenter";
  ApiController apiController = new ApiController.internal();

  getAboveRow(List<dynamic> pendingDataList, int index) {

    return Row(
      children: <Widget>[
        Expanded(
          flex: 3,
          child: Container(
            alignment: Alignment.centerLeft,
            width: 100.0,
            height: 100.0,
            child: Image.network(
              pendingDataList[index][DBConstant.IMAGE_URL],
              fit: BoxFit.cover,
            ),
          ),
        ),
        Expanded(
          flex: 7,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                pendingDataList[index][DBConstant.PRODUCT_NAME],
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.bold),
                    ),
              SizedBox(
                height: 5,
              ),
              Text(
                pendingDataList[index][DBConstant.BAR_CODE],
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 14,
                    fontWeight: FontWeight.normal),
              ),
              new Row(
                children: [
                  Expanded(
                    flex: 4,
                    child: Text(
                      "Ord Qty. " +
                          pendingDataList[index][DBConstant.ORDER_QUANTITY],
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 14,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Expanded(
                      flex: 6,
                      child: new Column(
                        children: [
                          InkWell(
                            onTap: () {
/*
                              String offer_type = GlobalWidget.getDatafromJson(
                                  pendingDataList[current_index]
                                  [DBConstant.PRODUCT_DATA],
                                  "offer_type");*/

                              if (active != 1) {

                                current_index = index;
                                if (active == 2) {
                                  Undodaat(
                                      retrivedDataList[index][DBConstant.ID]
                                          .toString(),
                                      pending,
                                      retrivedDataList[index]
                                      [DBConstant.PRODUCT_ID]
                                          .toString(), GlobalWidget.getDatafromJson(
                                      retrivedDataList[index]
                                      [DBConstant.PRODUCT_DATA],
                                      "offer_type"));
                                } else {
                                  Undodaat(
                                      notFoundDataList[index][DBConstant.ID]
                                          .toString(),
                                      pending,
                                      notFoundDataList[index]
                                      [DBConstant.PRODUCT_ID]
                                          .toString(), GlobalWidget.getDatafromJson(
                                      notFoundDataList[index]
                                      [DBConstant.PRODUCT_DATA],
                                      "offer_type"));
                                }
                              }
                            },
                            child: active == 1
                                ? Text(
                                    "Stock : " +
                                        pendingDataList[index]
                                            [DBConstant.STOCK_QUANTITY],
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold),
                                  )
                                : getUndoText(index,active)? Text(
                                    "UNDO",
                                    style: TextStyle(
                                        color: Colors.deepOrange,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ):new Container(),
                          ),
                          new InkWell(
                            onTap: () {
                              current_index = index;
                              String val = GlobalWidget.getDatafromJson(
                                  pendingDataList[index]
                                      [DBConstant.PRODUCT_DATA], "cart_offer_id");
                              String parent_product_id = GlobalWidget.getDatafromJson(
                                      pendingDataList[index][DBConstant.PRODUCT_DATA],
                                      "parent_product_id");
                              String srt_message = GlobalWidget.getDatafromJson(
                                  pendingDataList[index]
                                      [DBConstant.PRODUCT_DATA],
                                  "srt_message");
                              String offer_type = GlobalWidget.getDatafromJson(pendingDataList[index][DBConstant.PRODUCT_DATA],
                                  "offer_type");
                              Utility.log(TAG, offer_type);
                              Utility.log(TAG, parent_product_id);
                              Utility.log(TAG, val);
                              if (active != 1) {
                              } else if (widget.lock == "0") {
                                GlobalWidget.showToast(context, Lockmsg);
                              } else if (val != "0") {
                                GlobalWidget.showToast(
                                    context, "cart item can not short");
                              }else if(offer_type == "3"||offer_type=="4")
                                {
                                  UpdateSortQty(
                                      pendingDataList[index][DBConstant.ID]
                                          .toString(),
                                      pendingDataList[index]
                                      [DBConstant.ORDER_QUANTITY]
                                          .toString());
                                } else if (parent_product_id != "0") {
                               /* if(parent_product_id == pendingDataList[index][DBConstant.PRODUCT_ID].toString())
                                {
                                  UpdateSortQty(
                                      pendingDataList[index][DBConstant.ID]
                                          .toString(),
                                      pendingDataList[index]
                                      [DBConstant.ORDER_QUANTITY]
                                          .toString());
                                }else
                                {
                                  GlobalWidget.showToast(context, srt_message);
                                }
*/                                   GlobalWidget.showToast(context, srt_message);
                              } else {
                                UpdateSortQty(
                                    pendingDataList[index][DBConstant.ID]
                                        .toString(),
                                    pendingDataList[index]
                                            [DBConstant.ORDER_QUANTITY]
                                        .toString());
                              }
                            },
                            child: new Container(
                              margin: EdgeInsets.all(2.0),
                              padding: EdgeInsets.all(8.0),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10.0),
                                  border: Border.all(
                                      width: 1,
                                      color: Colors.black,
                                      style: BorderStyle.solid)),
                              child: Text(
                                "Srt Qty : " +
                                    pendingDataList[index]
                                        [DBConstant.SORT_QUANTITY],
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          )

                        ],
                      ))
                ],
              ),
              SizedBox(
                height: 3,
              ),
              getOfferTextText(index,active)?new Container(
                margin: EdgeInsets.only(bottom: 10.0),
                width:MediaQuery.of(context).size.width/3,
                child:  Text("Offer Product",textAlign: TextAlign.center,style: TextStyle(color: Colors.green[300],fontSize: 14.0),),
              ):new Container()
            ],
          ),
        ),
      ],
    );
  }

  Widget getOrderList() {
    if (active == 1) {
      return _pendingOrderList();
    } else if (active == 2) {
      return _retrivedOrderList();
    } else {
      return _notFoundOrderList();
    }
    /*
    switch(active)
    {
      case 1:
        return

        break;

      case 2:
        return
          new Container(
            height: MediaQuery.of(context).size.height-60,
            width: MediaQuery.of(context).size.width,
            child: _retrivedOrderList(),

          );
        break;

      case 3:
      return
        new Container(
          height: MediaQuery.of(context).size.height-60,
          width: MediaQuery.of(context).size.width,
          child: _notFoundOrderList(),

        );
        break;

    }*/
  }

  Future<void> getretrived() async {
    retrivedDataList = new List();
    List a2 = await DatabaseHelper.db.getAllPendingProduts(orderId, retrived);
    for (int i = 0; i < a2.length; i++) {
      setState(() {
        retrivedDataList.add(a2[i]);
      });
    }
  }

  Future<void> getPending() async {
    pendingDataList = new List();
    List a1 = await DatabaseHelper.db.getAllPendingProduts(orderId, pending);
    for (int i = 0; i < a1.length; i++) {
      setState(() {
        pendingDataList.add(a1[i]);
      });
    }
  }

  int itemcount = 0;

  Future<void> getPendingCounter() async {
    pendingDataList = new List();
    List a1 = await DatabaseHelper.db.getAllPendingProdutsCount(orderId);
    setState(() {
      itemcount = a1.length;
    });
  }

  Future<void> getnotfound() async {
    notFoundDataList = new List();

    List a3 = await DatabaseHelper.db.getAllPendingProduts(orderId, not_found);
    for (int i = 0; i < a3.length; i++) {
      setState(() {
        notFoundDataList.add(a3[i]);
      });
    }
  }

  String Lockmsg = "Please lock order first.";
  String LockmsgOther = "Order assigned to other.";

  Future<void> update(String retrived1, int index,String offer_type) async
  {
    int id = await DatabaseHelper.db.updateRetrivedNotFoundInSRTTable(
        pendingDataList[index][DBConstant.ID].toString(), retrived1);
    if (offer_type!="4")
    {
      int id1 = await DatabaseHelper.db.updateNotFoundInChildable(
          pendingDataList[index][DBConstant.ORDER_ID].toString(),
          retrived1,
          pendingDataList[index][DBConstant.PRODUCT_ID].toString());
    }
    setState(() {
      if (active == 1) {
        pendingDataList.removeAt(index);
      }
      getPending();
      getretrived();
      getnotfound();
    });
  }

  Future<void> updatenotFound(String retrived1, int index,String offer_type) async
  {
    int id = await DatabaseHelper.db.updateNotFoundInSRTTable(
        pendingDataList[index][DBConstant.ID].toString(), retrived1);


    if (offer_type!="4")
    {
      int id1 = await DatabaseHelper.db.NotFoundInChildable(
          pendingDataList[index][DBConstant.ORDER_ID].toString(),
          retrived1,
          pendingDataList[index][DBConstant.PRODUCT_ID].toString());
    }
    setState(() {
      if (active == 1) {
        pendingDataList.removeAt(index);
      }
      getPending();
      getretrived();
      getnotfound();
    });
  }

  Future<void> Undodaat(String id_val, String type, String Productid,String offer_type) async
  {
    int id = await DatabaseHelper.db.UndoFunction(id_val, type);

    if(offer_type!="4")
      {
        int id1 = await DatabaseHelper.db
            .updateNotFoundInChildable(orderId, type, Productid);

      }

    setState(() {
      if (active == 2) {
        retrivedDataList.removeAt(current_index);
      } else {
        notFoundDataList.removeAt(current_index);
      }
      getPending();
      getretrived();
      getnotfound();
    });
  }

  Future<void> locked() async {
    orderId = (await Utility.getStringPreference(Constants.ORDER_ID));
    String storeId = (await Utility.getStringPreference(Constants.STORE_CODE));
    String userId = (await Utility.getStringPreference(Constants.USER_ID));

    if (await NetworkCheck.check()) {
      var xml = "<order_info>\n " +
          "<empcode>" +
          await Utility.getStringPreference(Constants.EMPLOYEE_ID) +
          "</empcode>\n " +
          "<tat>" +
          widget.delivery_date.toString() +
          "/" +
          widget.delivery_time.toString() +
          "</tat>\n " +
          "<employee_id>" +
          await Utility.getStringPreference(Constants.EMPLOYEE_ID) +
          "</employee_id>\n " +
          "<user_id>" +
          userId +
          "</user_id>\n " +
          "<coco_id>" +
          storeId +
          "</coco_id>\n " +
          "<order_id>" +
          orderId +
          "</order_id>\n" +
          "</order_info>";
      Utility.log(TAG, "ResponseSend: " + xml);
      apiController
          .postXmlDataWithHeader(APIConstant.LockOrder, xml)
          .then((String apiResponse) {
        Utility.log(TAG, "ResponseSendval: " + apiResponse);
        LoginResponse response = LoginResponse.fromJson(apiResponse);
        if (response.status == true) {
          Toast.show("Order Locked Successfully", context,
              duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
          UpdateLockinLocal(orderId);
          setState(() {
            widget.lock = "1";
          });
        } else {
          Toast.show("Order Alraedy Locked", context,
              duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
        }
      }).catchError((error) {
        Utility.log(TAG, error.toString());
      });
    } else {}
  }

  Future<void> getLockstatus() async {
    if (widget.lock == "1") {
      Toast.show("Order already Locked...", context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    } else {
      locked();
    }
  }
  Future<void> UpdateSortQty(String id_val, String qty) async {

    bool status = await DatabaseHelper.db.getLockedStatus(orderId);
    final myController = TextEditingController();
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //this right here
            child: Container(
              height: 200,
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Update Sort Quantity ",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                          fontSize: 20.0),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    TextField(
                      controller: myController,
                      decoration:
                          InputDecoration(hintText: 'Enter Sort Quantity ?'),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    SizedBox(
                      width: 320.0,
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          RaisedButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text(
                              "Cancel",
                              style: TextStyle(color: Colors.white),
                            ),
                            color: Colors.red,
                          ),
                          SizedBox(
                            width: 20.0,
                          ),
                          RaisedButton(
                            onPressed: () {
                              if (myController.text.toString().length <= 0) {
                                Toast.show(
                                    "Quantity should not be blank!", context,
                                    duration: Toast.LENGTH_LONG,
                                    gravity: Toast.CENTER);
                              } else if (int.parse(qty) <=
                                  int.parse(myController.text.toString())) {
                                Toast.show(
                                    "Sort quantity should not be greater than or equal to  Order quantity!",
                                    context,
                                    duration: Toast.LENGTH_LONG,
                                    gravity: Toast.CENTER);
                              } else {
                                UpdateSortQtyFun(
                                    id_val, myController.text.toString());
                                Navigator.of(context).pop();
                              }
                            },
                            child: Text(
                              "Save",
                              style: TextStyle(color: Colors.white),
                            ),
                            color: const Color(0xFF1BC0C5),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }

  Future<void> UpdateSortQtyFun(String id_val, String qty) async {
    //dlgkfj
    int id = await DatabaseHelper.db.SrtQtyFunction(id_val, qty);
    String pro_id=pendingDataList[current_index][DBConstant.PRODUCT_ID];
    for(int i=0;i<pendingDataList.length;i++)
      {
        String parent_product_id = GlobalWidget.getDatafromJson(pendingDataList[i][DBConstant.PRODUCT_DATA], "parent_product_id");
        String offer_type = GlobalWidget.getDatafromJson(pendingDataList[i][DBConstant.PRODUCT_DATA], "offer_type");
        if(pro_id==parent_product_id&&offer_type!="4")
          {
            String get_qty = GlobalWidget.getDatafromJson(pendingDataList[i][DBConstant.PRODUCT_DATA], "get_qty");
            int child_qty=int.parse(get_qty)*int.parse(qty);
            int id1 = await DatabaseHelper.db.SrtQtyFunctionChildProduct(
                pendingDataList[current_index][DBConstant.ORDER_ID],
                child_qty.toString(),
                pendingDataList[current_index][DBConstant.PRODUCT_ID]);
          }
      }
    getPending();
  }

  Future<void> UpdateDone() async {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //this right here
            child: Container(
              height: 200,
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Are you sure to done this order",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                          fontSize: 20.0),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    SizedBox(
                      width: 320.0,
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          RaisedButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text(
                              "NO",
                              style: TextStyle(color: Colors.white),
                            ),
                            color: Colors.red,
                          ),
                          SizedBox(
                            width: 20.0,
                          ),
                          RaisedButton(
                            onPressed: () {
                              UpdateFinal();

                              Navigator.of(context).pop();
                            },
                            child: Text(
                              "YES",
                              style: TextStyle(color: Colors.white),
                            ),
                            color: const Color(0xFF1BC0C5),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }

  Future<void> UpdateLockinLocal(String orderId) async {
    bool status = await DatabaseHelper.db.updateLockedStatus(orderId);
  }

  Future<void> UpdateFinal() async {
    List a1 = await DatabaseHelper.db.getAllPendingProdutsCount(orderId);
    setState(() {
      itemcount = a1.length;
    });

    List students = new List();

    orderId = (await Utility.getStringPreference(Constants.ORDER_ID));
    String storeId = (await Utility.getStringPreference(Constants.STORE_CODE));
    String userId = (await Utility.getStringPreference(Constants.USER_ID));
    String EMPLOYEE_ID = (await Utility.getStringPreference(Constants.EMPLOYEE_ID));

    bool pending_remain = false;
    for (int i = 0; i < a1.length; i++) {
      if (a1[i][DBConstant.PRODUCT_TYPE] == pending) {
        pending_remain = true;
        break;
      }

      String Product_Data = a1[i][DBConstant.PRODUCT_DATA].toString();
      var datval = json.decode(Product_Data);
      String is_found = "0";
      if (a1[i][DBConstant.PRODUCT_TYPE] == retrived) {
        is_found = "1";
      }

      Map<String, dynamic> map() => {
            'product_id': datval['product_id'].toString(),
            'cart_offer_id': datval['cart_offer_id'].toString(),
            'parent_product_id': datval['parent_product_id'].toString(),
            'c_offer_id': datval['c_offer_id'].toString(),
            'custom_product_id': orderId +
                datval['product_id'].toString() +
                datval['parent_product_id'].toString() +
                datval['is_offer'].toString() +
                datval['c_offer_id'].toString(),
            'is_offer': datval['is_offer'].toString(),
            'offer_type': datval['offer_type'].toString(),
            'is_found': is_found,
            'retrieval_status': "0",
            'sort_quantity': a1[i][DBConstant.SORT_QUANTITY].toString(),
            'order_quantity':
                a1[i][DBConstant.ORDER_QUANTITY].toString().toString(),
          };

      String result = jsonEncode(map());
      students.add(result);
    }

    if (pending_remain == true) {
      Toast.show(
          "Some products are still remain to retrieve. Get it into basket than press done",
          context,
          duration: Toast.LENGTH_LONG,
          gravity: Toast.BOTTOM);
    } else {
      Map<String, dynamic> map() => {
            "\"" + 'order_id' + "\"": "\"" + orderId.toString() + "\"",
            "\"" + 'user_id' + "\"": "\"" + userId.toString() + "\"",
            "\"" + 'employee_id' + "\"": "\"" + EMPLOYEE_ID.toString() + "\"",
            "\"" + 'product' + "\"": students.toString(),
          };

      apiController.PostsNew(APIConstant.OrderDone, map().toString())
          .then((apiResponse) async {
        var data = apiResponse;
        var data1 = json.decode(data.body);
        Utility.log(TAG, "Response: " + data1.toString());
        Utility.log(TAG, "Api " + data1['status'].toString());

        Toast.show(data1['message'], context,
            duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
        if (data1['status'] == true) {
          int status = await DatabaseHelper.db.deleteProductFromAllTabele(orderId);
          Utility.log(TAG + "Delete", status);
          if (status != 0) {
            Navigator.of(context).pop();
            Navigator.of(context).pop();
          }
        }
      });
    }
  }

  Future<void> retriveactionOfchild() async {

    String offer_type = GlobalWidget.getDatafromJson(
        pendingDataList[current_index]
        [DBConstant.PRODUCT_DATA],
        "offer_type");
    String parent_product_id = GlobalWidget.getDatafromJson(
        pendingDataList[current_index]
        [DBConstant.PRODUCT_DATA],
        "parent_product_id");
    Utility.log(TAG,"productids $current_index $offer_type $parent_product_id");
    bool status = await DatabaseHelper.db.getRetrived(orderId,parent_product_id, retrived);
    if (status == true) {
      update(retrived, current_index,offer_type);
    } else if(offer_type=="3"||offer_type=="4")
    {
      update(retrived, current_index,offer_type);
    } else {
      String srt_message = GlobalWidget.getDatafromJson(
          pendingDataList[current_index][DBConstant.PRODUCT_DATA],
          "retrieve_message");
      GlobalWidget.showToast(context, srt_message);
    }
  }

  getUndoText(int index, int active) {
    bool value=true;
    String offer_type,parent_product_id,is_offer;
    int i_offer_type,i_parent_product_id,i_is_offer;
    if(active==2)
      {

     offer_type= GlobalWidget.getDatafromJson(
            retrivedDataList[index]
            [DBConstant.PRODUCT_DATA],
            "offer_type");

     is_offer= GlobalWidget.getDatafromJson(
            retrivedDataList[index]
            [DBConstant.PRODUCT_DATA],
            "is_offer");

     parent_product_id = GlobalWidget.getDatafromJson(
            retrivedDataList[index]
            [DBConstant.PRODUCT_DATA],
            "parent_product_id");


      }else if(active==3)
        {
          offer_type= GlobalWidget.getDatafromJson(
              notFoundDataList[index]
              [DBConstant.PRODUCT_DATA],
              "offer_type");

          parent_product_id = GlobalWidget.getDatafromJson(
              notFoundDataList[index]
              [DBConstant.PRODUCT_DATA],
              "parent_product_id");

          is_offer= GlobalWidget.getDatafromJson(
              notFoundDataList[index]
              [DBConstant.PRODUCT_DATA],
              "is_offer");
        }
    try
    {

      i_offer_type=int.parse(offer_type.toString());
      i_is_offer=int.parse(is_offer.toString());
      i_parent_product_id=int.parse(parent_product_id.toString());
      Utility.log(TAG, i_offer_type.toString()+"  pid "+i_parent_product_id.toString()+"  "+i_is_offer.toString());
      if(i_is_offer==1&&i_parent_product_id>0)
        {
          if(i_offer_type==1)
            {
              value=false;
            } if(i_offer_type==2)
            {
              value=false;
            } if(i_offer_type==5)
            {
              value=false;
            }
        }
    }catch(e)
    {

    }
    return value;
  }
  getOfferTextText(int index, int active) {
    bool value=false;
    String offer_type,parent_product_id,is_offer;
    int i_offer_type,i_parent_product_id,i_is_offer;
    if(active==1)
    {

      offer_type= GlobalWidget.getDatafromJson(
          pendingDataList[index]
          [DBConstant.PRODUCT_DATA],
          "offer_type");

      is_offer= GlobalWidget.getDatafromJson(
          pendingDataList[index]
          [DBConstant.PRODUCT_DATA],
          "is_offer");

      parent_product_id = GlobalWidget.getDatafromJson(
          pendingDataList[index]
          [DBConstant.PRODUCT_DATA],
          "parent_product_id");


    }
    if(active==2)
      {

     offer_type= GlobalWidget.getDatafromJson(
            retrivedDataList[index]
            [DBConstant.PRODUCT_DATA],
            "offer_type");

     is_offer= GlobalWidget.getDatafromJson(
            retrivedDataList[index]
            [DBConstant.PRODUCT_DATA],
            "is_offer");

     parent_product_id = GlobalWidget.getDatafromJson(
            retrivedDataList[index]
            [DBConstant.PRODUCT_DATA],
            "parent_product_id");


      }else if(active==3)
        {
          offer_type= GlobalWidget.getDatafromJson(
              notFoundDataList[index]
              [DBConstant.PRODUCT_DATA],
              "offer_type");

          parent_product_id = GlobalWidget.getDatafromJson(
              notFoundDataList[index]
              [DBConstant.PRODUCT_DATA],
              "parent_product_id");

          is_offer= GlobalWidget.getDatafromJson(
              notFoundDataList[index]
              [DBConstant.PRODUCT_DATA],
              "is_offer");
        }
    try
    {

      i_offer_type=int.parse(offer_type.toString());
      i_is_offer=int.parse(is_offer.toString());
      i_parent_product_id=int.parse(parent_product_id.toString());
      Utility.log(TAG, i_offer_type.toString()+"  pid "+i_parent_product_id.toString()+"  "+i_is_offer.toString());
      if(i_is_offer==1&&i_parent_product_id>0)
        {/*
          if(i_offer_type==1)
            {
              value=false;
            } if(i_offer_type==2)
            {
              value=false;
            } if(i_offer_type==5)
            {
              value=false;
            }*/

          value=true;
        }
    }catch(e)
    {

    }
    return value;
  }
}

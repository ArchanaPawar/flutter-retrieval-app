import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:retrivalapp/utils/AppColor.dart';
import 'package:toast/toast.dart';

class GlobalWidget
{
  static getpadding() {
    return EdgeInsets.all(2);
  }

  static getpaddingBtn() {
    return EdgeInsets.only(left: 20,right: 20);
  }

  static getDecoration(bool value) {
    return new BoxDecoration(
      color: value==true?Colors.red:Colors.grey,
      shape: BoxShape.circle,
    );
  }

  static getBoxConstraint() {
    return BoxConstraints(
      minWidth: 22,
      minHeight: 22,
    );
  }

  static getTextStyle(bool val) {

    return new TextStyle(
      color: Colors.white,
      fontSize: 12,
    );
  }

  static getTextStyleBottomText(bool val) {
    return new TextStyle(
      color:val==true?colorPrimaryDark: Colors.grey,
      fontSize: 10,
    );
  }

  static getSize() {
    return 35.0;
  }

  static getcolor(bool val) {
    if(val==true)
      {
        return colorPrimaryDark;
        return new Color(0xFFFF0000);
      }else{

      return Colors.grey;
      return new Color(0xFF008000);
    }
  }

  static getHeadButtonShape(MaterialColor green) {
    return BoxDecoration(
        color: green.shade900,
        borderRadius: BorderRadius.all(Radius.circular(10.0)));
  }

  static getMargin() {
    return  EdgeInsets.only(left: 10,right: 10,top: 12,bottom: 12);
  }

  static void showToast(BuildContext context, String errorMessage )
  {
    Toast.show(errorMessage, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);;
  }
  static String getDatafromJson(pendingDataList, String s)
  {
        String Product_Data = pendingDataList;
        var datval = json.decode(Product_Data);
        return datval[s];
  }

}
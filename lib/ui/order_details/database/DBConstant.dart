class DBConstant {
  static final String DATA_BASE_NAME = "retrieval_db";
  static   final String TABLE_ALL_PRODUCT = "All_Product";
  static final String TABLE_SRT = "tbl_Sort_Quantity";
  static final String ORDER_ID = "ORDER_ID";
  static final String PRODUCT_DATA = "PRODUCT_DATA";
  static final String PRODUCT_TYPE = "PRODUCT_TYPE";
  static final String PRODUCT_ID = "PRODUCT_ID";
  static final String ID = "id";
  static final String CUSTOM_PRODUCT_ID = "CUSTOM_PRODUCT_ID";
  static final String C_OFFER_ID = "C_OFFER_ID";
  static final String CART_OFFER_ID = "CART_OFFER_ID";
  static final String PARENT_PRODUCT_ID = "PARENT_PRODUCT_ID";
  static final String PRODUCT_NAME = "PRODUCT_NAME";
  static final String IMAGE_URL = "IMAGE_URL";
  static final String BAR_CODE = "BAR_CODE";
  static final String SORT_QUANTITY = "SORT_QUANTITY";
  static final String PRODUCT_QUANTITY = "PRODUCT_QUANTITY";
  static final String ORDER_QUANTITY = "ORDER_QUANTITY";
  static final String IS_FOUND = "IS_FOUND";
  static final String PRICE = "PRICE";
  static final String MAX_QUANTITY_PERCENT = "MAX_QUANTITY_PERCENT";
  static final String STOCK_QUANTITY = "STOCK_QUANTITY";
  static final String REG_QUANTITY = "REG_QUANTITY";
  static final String TOTAL = "TOTAL";
  static final String OFFER_TYPE = "OFFER_TYPE";
  static final String BUY_QUANTITY = "BUY_QUANTITY";
  static final String GET_QUANTITY = "GET_QUANTITY";
  static final String IS_COMBO = "IS_COMBO";
  static final String NF_MESSAGE = "NF_MESSAGE";
  static final String SRT_MESSAGE = "SRT_MESSAGE";
  static final String RETRIEVE_MESSAGE = "RETRIEVE_MESSAGE";
  static final String PARENT_RETRIEVE_MESSAGE = "PARENT_RETRIEVE_MESSAGE";
  static final String PARENT_PRODUCT_NAME = "PARENT_PRODUCT_NAME";
  static final String RETRIEVAL_STATUS = "RETRIEVAL_STATUS";
  static final String IS_OFFER_PRODUCT = "IS_OFFER_PRODUCT";
  static final String CREATED_TIMESTAMP = "CREATED_TIMESTAMP";
  static final String IS_LOCK = "IS_LOCK";


  static final String CREATE_All_PRODUCT_TABLE = "CREATE TABLE " +
      TABLE_ALL_PRODUCT +
      "(" +
      ID+" INTEGER PRIMARY KEY AUTOINCREMENT , " +
      PRODUCT_ID +
      " TEXT," +
      PARENT_PRODUCT_ID +
      " TEXT," +
      ORDER_ID +
      " TEXT," +
      PRODUCT_NAME +
      " TEXT," +
      IMAGE_URL +
      " TEXT," +
      BAR_CODE +
      " TEXT," +
      ORDER_QUANTITY +
      " TEXT," +
      TOTAL +
      " TEXT," +
      STOCK_QUANTITY +
      " TEXT," +
      SORT_QUANTITY +
      " TEXT," +
      PRODUCT_DATA +
      " TEXT," +
      IS_LOCK +
      " TEXT," +
       PRODUCT_TYPE+
      " TEXT )";


}


import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:retrivalapp/ui/order_details/model/ProductDetails.dart';
import 'package:retrivalapp/ui/order_details/model/ProductModel.dart';
import 'package:retrivalapp/utils/Utility.dart';
import 'package:sqflite/sqflite.dart';

import 'DBConstant.dart';

class DatabaseHelper {
  DatabaseHelper._();

  static final DatabaseHelper db = DatabaseHelper._();

  Database _database;

  Future<Database> get dataBaseInstance async {
    if (_database != null) return _database;
    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  //init data base
  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, DBConstant.DATA_BASE_NAME);
    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
          await db.execute(DBConstant.CREATE_All_PRODUCT_TABLE);
        });
  }

  //Clear database
  clearDatabase() async {
    try{
      final db = await dataBaseInstance;
      /*  db.rawDelete("DELETE FROM sqlite_sequence WHERE name =  "+DBConstant.TABLE_PENDING_PRODUCT);

      db.rawDelete("DELETE FROM "+DBConstant.TABLE_RETRIEVAL_PRODUCT);
      db.rawDelete("DELETE FROM sqlite_sequence WHERE name =  "+DBConstant.TABLE_RETRIEVAL_PRODUCT);

      db.rawDelete("DELETE FROM "+DBConstant.TABLE_SRT);
      db.rawDelete("DELETE FROM sqlite_sequence WHERE name =  "+DBConstant.TABLE_SRT);
*/

      db.rawDelete("DELETE FROM "+DBConstant.TABLE_ALL_PRODUCT);
    //  db.rawDelete("DELETE FROM sqlite_sequence WHERE name =  "+DBConstant. result[i][DBConstant.IMAGE_URL]);

      /**
       * close database
       */
      //db.close();

    } catch(error){
      throw Exception('DatabaseHelper.clearDatabase: ' + error.toString());
    }
  }


//Add product in retrieval table
  Future<int> addProductAllTable(String productId, String custom_id, String cart_offer_id, String c_offer_id, String parent_product_id, String orderId, String productName, String imageUrl, String barcode, String price, String orderQuantity, String maxQuantity, String stockQuantity, String total, String offerType, String buyQuantity, String getQuantity, String isCombo, String nfMessage, String srtMessage, String retrieveMessage, String parentRetrieveMessage, String parent_product_name, String regQuantity, String retrievalStatus, String sortQuantity, String isOfferProduct) async {
    int id = 0;
    try{
      final db = await dataBaseInstance;
      // row to insert
      Map<String, dynamic> row = {
        DBConstant.PRODUCT_ID: productId,
        DBConstant.CUSTOM_PRODUCT_ID: custom_id,
        DBConstant.CART_OFFER_ID: cart_offer_id,
        DBConstant.C_OFFER_ID: c_offer_id,
        DBConstant.PARENT_PRODUCT_ID: parent_product_id,
        DBConstant.ORDER_ID: orderId,
        DBConstant.PRODUCT_NAME: productName,
        DBConstant.IMAGE_URL: imageUrl,
        DBConstant.BAR_CODE: barcode,
        DBConstant.SORT_QUANTITY: sortQuantity,
        DBConstant.ORDER_QUANTITY: orderQuantity,
        DBConstant.IS_FOUND: "0",
        DBConstant.PRICE: price,
        DBConstant.MAX_QUANTITY_PERCENT: maxQuantity,
        DBConstant.STOCK_QUANTITY: stockQuantity,
        DBConstant.TOTAL: total,
        DBConstant.OFFER_TYPE: offerType,
        DBConstant.BUY_QUANTITY: buyQuantity,
        DBConstant.GET_QUANTITY: getQuantity,
        DBConstant.IS_COMBO: isCombo,
        DBConstant.NF_MESSAGE: nfMessage,
        DBConstant.SRT_MESSAGE: srtMessage,
        DBConstant.RETRIEVE_MESSAGE: retrieveMessage,
        DBConstant.PARENT_RETRIEVE_MESSAGE: parentRetrieveMessage,
        DBConstant.PARENT_PRODUCT_NAME: parent_product_name,
        DBConstant.REG_QUANTITY: regQuantity,
        DBConstant.RETRIEVAL_STATUS: retrievalStatus,
        DBConstant.IS_OFFER_PRODUCT: isOfferProduct,
        DBConstant.PRODUCT_TYPE: "Pending",
      };
      id = await db.insert(DBConstant.TABLE_ALL_PRODUCT, row);

      /**
       * close database
       */
      //db.close();
    } catch(error){
      throw Exception('DatabaseHelper.addProductInRetrievalTable: ' + error.toString());
    }

    return id;

  }



  //Add product in all table
  Future<int> addProductInAllTable(
      String productId,
      String orderId,
      String PRODUCT_NAME,
      String orderQuantity,
      String Parent_Product_Id,
      String IMAGE_URL,
      String BAR_CODE,
      String ORDER_QUANTITY,
      String TOTAL,
      String STOCK_QUANTITY,
      String ProductData,
      ) async{

    int id = 0;
    try{final db = await dataBaseInstance;

    // row to insert
    Map<String, dynamic> row = {
      DBConstant.PRODUCT_ID: productId,
      DBConstant.ORDER_ID: orderId,
      DBConstant.PRODUCT_NAME: PRODUCT_NAME,
      DBConstant.SORT_QUANTITY: "0",
      DBConstant.PRODUCT_TYPE: "Pending",
      DBConstant.PARENT_PRODUCT_ID: Parent_Product_Id,
      DBConstant.IMAGE_URL: IMAGE_URL,
      DBConstant.BAR_CODE: BAR_CODE,
      DBConstant.ORDER_QUANTITY: ORDER_QUANTITY,
      DBConstant.TOTAL: TOTAL,
      DBConstant.PRODUCT_DATA: ProductData,
      DBConstant.STOCK_QUANTITY: STOCK_QUANTITY,
    };
    id = await db.insert(DBConstant.TABLE_ALL_PRODUCT, row);
      /**
       * close database
       */
      //db.close();
    } catch(error){
      throw Exception('DatabaseHelper.addProductInRetrievalTable: ' + error.toString());
    }


    return id;

  }


  //check product if already exist in pending table
  Future<bool> checkOrderIfAlreadyExist(String custom_product_id, String order_id, String isOfferProduct) async {
    bool isExist = false;
    try{
      final db = await dataBaseInstance;
      //String selectQuery = "SELECT  * FROM " + DBConstant.TABLE_ALL_PRODUCT + " WHERE " + DBConstant.ORDER_ID + " = '" + order_id + "' AND " + DBConstant.CUSTOM_PRODUCT_ID + "='" + custom_product_id + "' AND " + DBConstant.IS_OFFER_PRODUCT + "='" + isOfferProduct + "'";
      String selectQuery = "SELECT  * FROM " + DBConstant.TABLE_ALL_PRODUCT + " WHERE " + DBConstant.ORDER_ID + " = '" + order_id  + "'";
      List<Map> result = await db.rawQuery(selectQuery);
      if (result.length > 0) {
        isExist = true;
      }
      //db.close();
    } catch(error){
      throw Exception('DatabaseHelper.checkProductIfAlreadyExist: ' + error.toString());
    }

    return isExist;
  }


  //Delete product from pending table
  Future<int> deleteProductFromAllTabele( String order_id) async{
    int deleteId = 0;
    try {
      final db = await dataBaseInstance;

      //db.close();
      return await db.delete(DBConstant.TABLE_ALL_PRODUCT, where: DBConstant.ORDER_ID+' = ?', whereArgs: [order_id]);
    } catch(error){
      throw Exception('DbBase.deleteProductFromPending: ' + error.toString());
    }
    return deleteId;
  }



  //get retrieved product and update on server
  Future<List> getAllPendingProduts(String order_id,String Type) async
  {
    List arrayList = new List();
    try {
      final db = await dataBaseInstance;

    //  String selectQuery = "SELECT  * FROM " + DBConstant.TABLE_ALL_PRODUCT + " Where " + DBConstant.ORDER_ID + "='" + order_id + "' ORDER BY " + DBConstant.CREATED_TIMESTAMP + " DESC";
      String selectQuery = "SELECT  * FROM " + DBConstant.TABLE_ALL_PRODUCT + " Where "+DBConstant.PRODUCT_TYPE+" = '"+Type+"' AND " + DBConstant.ORDER_ID + "='" + order_id +"'";

      List<Map> result = await db.rawQuery(selectQuery);

      // looping through all rows and adding to list
      if (result.length > 0) {
        for (int i = 0; i < result.length; i++) {
          //arrayList.add( result[i][DBConstant.PRODUCT_DATA]);
          arrayList.add( result[i]);
        }
      }

      // close db connection
      //db.close();

    } catch(error){
      throw Exception('DbBase.getProductAndUpdateOnServer: ' + error.toString());
    }

    return arrayList;
  }

  //get retrieved product and update on server
  Future<bool> getLockedStatus(String order_id) async {
    bool status=false;
    try {
      final db = await dataBaseInstance;

    //  String selectQuery = "SELECT  * FROM " + DBConstant.TABLE_ALL_PRODUCT + " Where " + DBConstant.ORDER_ID + "='" + order_id + "' ORDER BY " + DBConstant.CREATED_TIMESTAMP + " DESC";
      String selectQuery = "SELECT  * FROM " + DBConstant.TABLE_ALL_PRODUCT + " Where "+DBConstant.IS_LOCK+" = '1' AND " + DBConstant.ORDER_ID + "='" + order_id +"'";

      List<Map> result = await db.rawQuery(selectQuery);
      if(result.length>0)
        {
          status=true;
        }

      // close db connection
      //db.close();

    } catch(error){
      throw Exception('DbBase.getProductAndUpdateOnServer: ' + error.toString());
    }

    return status;
  }

  //get retrieved product and update on server
  Future<List> getAllPendingProdutsCount(String order_id) async {
    List arrayList = new List();
    try {
      final db = await dataBaseInstance;

    //  String selectQuery = "SELECT  * FROM " + DBConstant.TABLE_ALL_PRODUCT + " Where " + DBConstant.ORDER_ID + "='" + order_id + "' ORDER BY " + DBConstant.CREATED_TIMESTAMP + " DESC";
      String selectQuery = "SELECT  * FROM " + DBConstant.TABLE_ALL_PRODUCT + " Where " + DBConstant.ORDER_ID + "='" + order_id +"'";

      List<Map> result = await db.rawQuery(selectQuery);

      // looping through all rows and adding to list
      if (result.length > 0) {
        for (int i = 0; i < result.length; i++) {
          //arrayList.add( result[i][DBConstant.PRODUCT_DATA]);
          arrayList.add( result[i]);
        }
      }

      Utility.log("taglength", result.length.toString());

      // close db connection
      //db.close();

    } catch(error){
      throw Exception('DbBase.getProductAndUpdateOnServer: ' + error.toString());
    }

    return arrayList;
  }

  updateShortQuantityInSRTTable(String orderId, String customProductId, String srt_quantity) async {
    try {
      final db = await dataBaseInstance;
      String selectQuery = "SELECT  * FROM " + DBConstant.TABLE_SRT + " WHERE " + DBConstant.CUSTOM_PRODUCT_ID + " = " + customProductId + " AND " + DBConstant.ORDER_ID + "='" + orderId + "' ORDER BY " + DBConstant.CREATED_TIMESTAMP + " DESC";
      List<Map> result = await db.rawQuery(selectQuery);

      if (result.length > 0) {
        Map<String, dynamic> values = {
          DBConstant.SORT_QUANTITY: srt_quantity,
        };

        int updateCount = await db.update(
            DBConstant.TABLE_SRT,
            values,
            where: '${DBConstant.CUSTOM_PRODUCT_ID} = ? AND '+DBConstant.ORDER_ID+' = ?',
            whereArgs: [customProductId,orderId]);

      } else {
        /**
         * insert as new row
         */
        Map<String, dynamic> row = {
          DBConstant.CUSTOM_PRODUCT_ID: customProductId,
          DBConstant.ORDER_ID: orderId,
          DBConstant.SORT_QUANTITY: srt_quantity
        };
        int id = await db.insert(DBConstant.TABLE_SRT, row);
      }
      // close db connection
      //db.close();

    } catch(error){
      throw Exception('DbBase.updateShortQuantityInSRTTable: ' + error.toString());
    }
  }


  updateRetrivedNotFoundInSRTTable(String row_id, String type) async {
    try {
      final db = await dataBaseInstance;

      Map<String, dynamic> values = {
        DBConstant.PRODUCT_TYPE: type,
      };

      int updateCount = await db.update(
          DBConstant.TABLE_ALL_PRODUCT,
          values,
          where: DBConstant.ID+' = ?',
          whereArgs: [row_id]);

      Utility.log("TAG", "Apiupdatecount " +updateCount.toString()+" "+row_id+"  "+"  "+type);
      // close db connection
      //db.close();

    } catch(error){
      throw Exception('DbBase.updateShortQuantityInSRTTable: ' + error.toString());
    }
  }
 updateNotFoundInSRTTable(String row_id, String type) async {
    try {
      final db = await dataBaseInstance;

      Map<String, dynamic> values = {
        DBConstant.PRODUCT_TYPE: type,
        DBConstant.SORT_QUANTITY: "0",
      };

      Utility.log("TAG", "updatedvalued " +values.toString());
      int updateCount = await db.update(
          DBConstant.TABLE_ALL_PRODUCT,
          values,
          where: DBConstant.ID+' = ?',
          whereArgs: [row_id]);

      Utility.log("TAG", "Apiupdatecount " +updateCount.toString()+" "+row_id+"  "+"  "+type);
      // close db connection
      //db.close();

    } catch(error){
      throw Exception('DbBase.updateShortQuantityInSRTTable: ' + error.toString());
    }
  }

  updateNotFoundInChildable(String order_id, String type,String product_id) async {
    try {
      final db = await dataBaseInstance;

      Map<String, dynamic> values = {
        DBConstant.PRODUCT_TYPE: type,
      };

      int updateCount = await db.update(
          DBConstant.TABLE_ALL_PRODUCT,
          values,
          where: '${DBConstant.ORDER_ID} = ? AND '+DBConstant.PARENT_PRODUCT_ID+' = ?',
          whereArgs: [order_id,product_id]);

      Utility.log("TAG", "Apiupdatecount " +updateCount.toString()+" "+order_id+"  "+"  "+type);
      // close db connection
      //db.close();

    } catch(error){
      throw Exception('DbBase.updateShortQuantityInSRTTable: ' + error.toString());
    }
  }
  NotFoundInChildable(String order_id, String type,String product_id) async {
    try {
      final db = await dataBaseInstance;
      Map<String, dynamic> values = {
        DBConstant.PRODUCT_TYPE: type,
        DBConstant.SORT_QUANTITY: "0",
      };

      int updateCount = await db.update(
          DBConstant.TABLE_ALL_PRODUCT,
          values,
          where: '${DBConstant.ORDER_ID} = ? AND '+DBConstant.PARENT_PRODUCT_ID+' = ?',
          whereArgs: [order_id,product_id]);

      Utility.log("TAG", "Apiupdatecount " +updateCount.toString()+" "+order_id+"  "+"  "+type);
      // close db connection
      //db.close();
    } catch(error){
      throw Exception('DbBase.updateShortQuantityInSRTTable: ' + error.toString());
    }
  }

  UndoFunction(String ListId, String type) async {
    try {
      final db = await dataBaseInstance;

      Map<String, dynamic> values = {
        DBConstant.PRODUCT_TYPE: type,
      };

      int updateCount = await db.update(
          DBConstant.TABLE_ALL_PRODUCT,
          values,
          where: "id = ?",
          whereArgs: [ListId]);

      Utility.log("TAG", "Apiupdatecount " +updateCount.toString()+" "+ListId+"  "+type);
      // close db connection
      //db.close();
    } catch(error){
      throw Exception('DbBase.updateShortQuantityInSRTTable: ' + error.toString());
    }
  }
  SrtQtyFunction(String ListId, String qty) async {
    try
    {
      final db = await dataBaseInstance;
      Map<String, dynamic> values = {
        DBConstant.SORT_QUANTITY: qty,
      };

      int updateCount = await db.update(
          DBConstant.TABLE_ALL_PRODUCT,
          values,
          where: "id = ?",
          whereArgs: [ListId]);

      Utility.log("TAG", "Apiupdatecount " +updateCount.toString()+" "+ListId+"  "+qty);
      // close db connection
      //db.close();
    } catch(error){
      throw Exception('DbBase.updateShortQuantityInSRTTable: ' + error.toString());
    }
  }

  SrtQtyFunctionChildProduct(String ListId, String qty,String Product_Id) async {
    try
    {
      final db = await dataBaseInstance;
      Map<String, dynamic> values = {
        DBConstant.SORT_QUANTITY: qty,
      };
      int updateCount = await db.update(
          DBConstant.TABLE_ALL_PRODUCT,
          values,
          where: '${DBConstant.ORDER_ID} = ? AND '+ DBConstant.PARENT_PRODUCT_ID+' = ?',
          whereArgs: [ListId,Product_Id]);

      Utility.log("TAG", "Apiupdatecount " +updateCount.toString()+" "+ListId+"  "+qty+"   "+Product_Id);
      // close db connection
      //db.close();

    } catch(error){
      throw Exception('DbBase.updateShortQuantityInSRTTable: ' + error.toString());
    }
  }
updateLockedStatus(String orderId) async {
    try {
      final db = await dataBaseInstance;

      Map<String, dynamic> values = {
        DBConstant.IS_LOCK: "1",
      };

      int updateCount = await db.update(
          DBConstant.TABLE_ALL_PRODUCT,
          values,
          where: DBConstant.ORDER_ID+' = ?',
          whereArgs: [orderId]);

      Utility.log("TAG", "Apiupdatecount " +updateCount.toString());
      // close db connection
      //db.close();

    } catch(error){
      throw Exception('DbBase.updateShortQuantityInSRTTable: ' + error.toString());
    }
  }


  Future<String> getSRTQuantity(String orderId, String customProductId) async {
    String srt_quantity = "0";
    try {
      final db = await dataBaseInstance;
      String selectQuery = "SELECT  * FROM " + DBConstant.TABLE_SRT + " WHERE " + DBConstant.CUSTOM_PRODUCT_ID + " = " + customProductId + " AND " + DBConstant.ORDER_ID + "='" + orderId + "' ORDER BY " + DBConstant.CREATED_TIMESTAMP + " DESC";
      List<Map> result = await db.rawQuery(selectQuery);
      if (result.length > 0) {
        srt_quantity =  result[0][DBConstant.SORT_QUANTITY];

      }
      // close db connection
      //db.close();

    } catch(error){
      throw Exception('DbBase.getSRTQuantity: ' + error.toString());
    }

    return srt_quantity;
  }
  Future<bool> getRetrived(String order_id, String parent_id,String type) async {
    bool result=false;
    try {
      final db = await dataBaseInstance;
      String selectQuery = "SELECT  * FROM " + DBConstant.TABLE_ALL_PRODUCT + " WHERE " + DBConstant.ORDER_ID + " = " + order_id + " AND " + DBConstant.PRODUCT_ID + "='" + parent_id + "'" + " AND " + DBConstant.PRODUCT_TYPE + "='" + type + "'";
      Utility.log("TAG", selectQuery);
      List<Map> result1 = await db.rawQuery(selectQuery);
      if (result1.length > 0)
      {
       result=true;
      }
      // close db connection
      //db.close();

    } catch(error){
      throw Exception('DbBase.getSRTQuantity: ' + error.toString());
    }

    return result;
  }

}
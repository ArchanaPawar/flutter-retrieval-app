
class ProductModel{
  String productId;
  String customProductId;
  String cartOfferId;
  String cOfferId;
  String parent_product_id;
  String productName;
  String imageUrl;
  String orderQuantity;
  String sortQuantity;
  String barCode;
  String isFound;
  String price;
  String maxQuantity;
  String stockQuantity;
  String total;
  String offerType;
  String buyQuantity;
  String getQuantity;
  String isCombo;
  String nfMessage;
  String srtMessage;
  String retrieveMessage;
  String parentRetrieveMessage;
  String parentProductName;
  String regQuantity;
  String retrievalStatus;
  String isOffer;

  ProductModel(
    this.productId,
    this.customProductId,
    this.cartOfferId,
    this.cOfferId,
    this.parent_product_id,
    this.productName,
    this.imageUrl,
    this.orderQuantity,
    this.sortQuantity,
    this.barCode,
    this.isFound,
    this.price,
    this.maxQuantity,
    this.stockQuantity,
    this.total,
    this.offerType,
    this.buyQuantity,
    this.getQuantity,
    this.isCombo,
    this.nfMessage,
    this.srtMessage,
    this.retrieveMessage,
    this.parentRetrieveMessage,
    this.parentProductName,
    this.regQuantity,
    this.retrievalStatus,
    this.isOffer
  );

}
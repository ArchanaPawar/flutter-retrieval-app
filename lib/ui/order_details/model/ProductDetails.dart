class ProductDetails {
  String imageUrl;
  String productId;
  String customProductId;
  String cartOfferId;
  String cOfferId;
  String parent_product_id;
  String name;
  String barcode;
  String price;
  String reg_qty;
  String max_qty_percent;
  String ord_qty;
  String stock_qty;
  String total;
  String offerType;
  String buyQuantity;
  String getQuantity;
  String isCombo;
  String nfMessage;
  String srtMessage;
  String retrieveMessage;
  String parentRetrieveMessage;
  String parentProductName;
  String r_status;
  String sortQuantity;
  String isOffer;

  ProductDetails (
  this.imageUrl,
  this.productId,
  this.customProductId,
  this.cartOfferId,
  this.cOfferId,
  this.parent_product_id,
  this.name,
  this.barcode,
  this.price,
  this.reg_qty,
  this.max_qty_percent,
  this.ord_qty,
  this.stock_qty,
  this.total,
  this.offerType,
  this.buyQuantity,
  this.getQuantity,
  this.isCombo,
  this.nfMessage,
  this.srtMessage,
  this.retrieveMessage,
  this.parentRetrieveMessage,
  this.parentProductName,
  this.r_status,
  this.sortQuantity,
  this.isOffer
  );

}
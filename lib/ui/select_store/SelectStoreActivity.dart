
import 'package:flutter/material.dart';
import 'package:retrivalapp/ui/home/HomeActivity.dart';
import 'package:retrivalapp/ui/order_details/database/DatabaseHelper.dart';
import 'package:retrivalapp/ui/primary_coco/PrimaryCocoListActivity.dart';
import 'package:retrivalapp/ui/select_store/StorePresenter.dart';
import 'package:retrivalapp/ui/select_store/StoreResonse.dart';
import 'package:retrivalapp/ui/select_store/StoreView.dart';
import 'package:retrivalapp/utils/AppColor.dart';
import 'package:retrivalapp/utils/Constants.dart';
import 'package:retrivalapp/utils/Utility.dart';
import 'package:retrivalapp/utils/appKeys.dart';
import 'package:toast/toast.dart';

class SelectStoreActivity extends StatefulWidget {
  SelectStoreActivity({ Key key }) : super(key: key);
  @override
  State createState() => StoreScreen();
}

class StoreScreen extends State<SelectStoreActivity> implements StoreView {
  final String TAG = "SelectStoreActivity";
  StorePresenter presenter;
  List<Datum> dataList;

  Widget appBarTitle = new Text("Select Store", style: new TextStyle(color: Colors.white),);
  Icon actionIcon = new Icon(Icons.search, color: Colors.white,);
  final key = new GlobalKey<ScaffoldState>();
  final TextEditingController searchQuery = new TextEditingController();
  bool isSearching;
  String searchText = "";


  searchListState() {
    searchQuery.addListener(() {
      if (searchQuery.text.isEmpty) {
        setState(() {
          isSearching = false;
          searchText = "";
        });
      }
      else {
        setState(() {
          isSearching = true;
          searchText = searchQuery.text;
        });
      }
    });
  }

  @override
  void initState() {
    super.initState();
    presenter = StorePresenter(this);
    presenter.getStoreList();
    isSearching = false;
    searchListState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: key,
      appBar: buildBar(context),
      body: dataList == null? Center(child: Utility.buildProgressIndicator(true),): ListView(
        padding: new EdgeInsets.symmetric(vertical: 8.0),
        children: isSearching ? _buildSearchList() : _buildList(),
      ),
    );
  }

  List<ChildItem> _buildList() {
    return dataList.map((Datum) => new ChildItem(Datum)).toList();
  }

  List<ChildItem> _buildSearchList() {
    if (searchText.isEmpty) {
      return dataList.map((Datum) => new ChildItem(Datum)).toList();
    }
    else {
      List<Datum> _searchList = List();
      for (int i = 0; i < dataList.length; i++) {
        String  name = dataList[i].storeName;
        if (name.toLowerCase().contains(searchText.toLowerCase())) {
          _searchList.add(dataList[i]);
        }
      }
      return _searchList.map((contact) => new ChildItem(contact))
          .toList();
    }
  }

  Widget buildBar(BuildContext context) {
    return new AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: colorPrimary,
        title: appBarTitle,
        actions: <Widget>[
          new IconButton(icon: actionIcon, onPressed: () {
            setState(() {
              if (this.actionIcon.icon == Icons.search) {
                this.actionIcon = new Icon(Icons.close, color: Colors.white,);
                this.appBarTitle = new TextField(
                  controller: searchQuery,
                  style: new TextStyle(
                    color: Colors.white,

                  ),
                  decoration: new InputDecoration(
                      prefixIcon: new Icon(Icons.search, color: Colors.white),
                      hintText: "Search Store...",
                      hintStyle: new TextStyle(color: Colors.white)
                  ),
                );
                _handleSearchStart();
              }
              else {
                _handleSearchEnd();
              }
            });
          },),
        ]
    );

  }

  void _handleSearchStart() {
    setState(() {
      isSearching = true;
    });
  }

  void _handleSearchEnd() {
    setState(() {
      this.actionIcon = new Icon(Icons.search, color: Colors.white,);
      this.appBarTitle =
      new Text("Select Store", style: new TextStyle(color: Colors.white),);
      isSearching = false;
      searchQuery.clear();
    });
  }




  @override
  void onFailed(String errorMessage) {
    Toast.show(errorMessage, context,
        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
  }

  @override
  void populateStoreList(List<Datum> data) {
    setState(() {
      this.dataList = data;
    });
  }
}

class ChildItem extends StatelessWidget {
  TextStyle style = TextStyle(color: Colors.grey, fontFamily: 'Montserrat', fontSize: 18.0);
  final Datum _datum;
  ChildItem(this._datum);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        onStoreSelected(context);
      },
      child: Card(
            child: Container(
              width: screenWidth(context),
              height: 38,
              margin: const EdgeInsets.only(left: 12,top: 28.0),
              child: Text(
                _datum.storeName,
                maxLines: 2,
                style:style ,
              ),
            ),
          ),
    );

  }

  void onStoreSelected(BuildContext context){
    /**
     * Clear database
     */
    try{
      DatabaseHelper.db.clearDatabase();
    } catch(error){
      throw Exception('SelectStoreActivity=====>Database=====> ' + error.toString());
    }


    /**
     * Hide keyboard
     */
    FocusScope.of(context).requestFocus(new FocusNode());

    Utility.setStringPreference(Constants.STORE_ID, _datum.storeId);
    Utility.setStringPreference(Constants.STORE_CODE, _datum.storeCode);
    Utility.setStringPreference(Constants.WMS_STORE_ID, _datum.wmsStoreId);
    Utility.setStringPreference(Constants.STORE_NAME, _datum.storeName);
    Utility.setStringPreference(Constants.LOCATION_ID, _datum.locationId);

    /**
     * Rest primary store details
     */
    Utility.setStringPreference(Constants.PRIMARY_STORE_ID, "");
    Utility.setStringPreference(Constants.PRIMARY_STORE_CODE, "");
    Utility.setStringPreference(Constants.PRIMARY_WMS_STORE_ID, "");
    Utility.setStringPreference(Constants.PRIMARY_STORE_NAME, "");
    Utility.setStringPreference(Constants.PRIMARY_LOCATION_ID, "");

    Toast.show(_datum.storeName, context,
        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);

    if(Utility.equalsIgnoreCase("JKWH",_datum.storeCode) || Utility.equalsIgnoreCase("OCO",_datum.storeCode)){
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => PrimaryCocoListActivity()));
    }else{
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeActivity()));
    }
  }



}

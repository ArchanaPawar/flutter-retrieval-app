
import 'package:retrivalapp/ui/select_store/StoreResonse.dart';

abstract class StoreView{
  void onFailed(String errorMessage);
  void populateStoreList(List<Datum> data);

}
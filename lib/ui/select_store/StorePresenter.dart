
import 'package:retrivalapp/ui/select_store/StoreResonse.dart';
import 'package:retrivalapp/utils/APIConstant.dart';
import 'package:retrivalapp/utils/ApiController.dart';
import 'package:retrivalapp/utils/Constants.dart';
import 'package:retrivalapp/utils/Message.dart';
import 'package:retrivalapp/utils/NetworkCheck.dart';
import 'package:retrivalapp/utils/Utility.dart';

import 'StoreView.dart';

class StorePresenter{
  final String TAG = "StorePresenter";
  StoreView view;
  StorePresenter(this.view);
  ApiController apiController = new ApiController.internal();

  void getStoreList() async {
    if (await NetworkCheck.check()) {
      var xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
          "<user_info>\n " +
          "<user_id>" + await Utility.getStringPreference(Constants.EMPLOYEE_ID) + "</user_id>\n" +
          "</user_info>";
      apiController.postXmlDataWithHeader(APIConstant.getStoreList, xml).then((String apiResponse) {
        Utility.log(TAG, "Response: " + apiResponse);
        StoreResponse response = StoreResponse.fromJson(apiResponse);
        if (response.status) {
          view.populateStoreList(response.data);
        } else {
          view.onFailed(response.message);
        }
      }).catchError((error) {
        Utility.log(TAG, error.toString());
        view.onFailed(Message.SOMETHING_WENT_WRONG);
      });
    } else {
      view.onFailed(Message.NO_INTERNET);
    }
    
  }
}

class OrderListModel{
  OrderListModel(
      this.orderId,
      this.itemCount,
      this.firstname,
      this.deliveryDate,
      this.deliveryTime,
      this.shippingAddress,
      this.vegCount,
      this.groCount,
      this.retrivalCount,
      this.pendingCount,
      this.notFoundCount,
      this.lockStatus,
      this.retrieveStatus,
      this.priorityOrder,
      );

  String orderId;
  String itemCount;
  String firstname;
  DateTime deliveryDate;
  String deliveryTime;
  String shippingAddress;
  int vegCount;
  int groCount;
  int retrivalCount;
  int pendingCount;
  String notFoundCount;
  int lockStatus;
  int retrieveStatus;
  String priorityOrder;



}
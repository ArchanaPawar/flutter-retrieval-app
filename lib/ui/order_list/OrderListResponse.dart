// To parse this JSON data, do
//
//     final pendingOrderResponse = pendingOrderResponseFromMap(jsonString);

import 'dart:convert';

class OrderListResponse {
  OrderListResponse({
    this.status,
    this.message,
    this.data,
  });

  bool status;
  String message;
  List<Datum> data;

  factory OrderListResponse.fromJson(String str) => OrderListResponse.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory OrderListResponse.fromMap(Map<String, dynamic> json) => OrderListResponse(
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
    data: json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromMap(x))),
  );

  Map<String, dynamic> toMap() => {
    "status": status == null ? null : status,
    "message": message == null ? null : message,
    "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toMap())),
  };
}

class Datum {
  Datum({
    this.orderId,
    this.itemCount,
    this.firstname,
    this.deliveryDate,
    this.deliveryTime,
    this.shippingAddress,
    this.vegCount,
    this.groCount,
    this.retrivalCount,
    this.pendingCount,
    this.notFoundCount,
    this.lockStatus,
    this.retrieveStatus,
    this.priorityOrder,
  });

  String orderId;
  String itemCount;
  String firstname;
  DateTime deliveryDate;
  String deliveryTime;
  String shippingAddress;
  int vegCount;
  int groCount;
  int retrivalCount;
  int pendingCount;
  String notFoundCount;
  int lockStatus;
  int retrieveStatus;
  String priorityOrder;

  factory Datum.fromJson(String str) => Datum.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Datum.fromMap(Map<String, dynamic> json) => Datum(
    orderId: json["order_id"] == null ? null : json["order_id"],
    itemCount: json["item_count"] == null ? null : json["item_count"],
    firstname: json["firstname"] == null ? null : json["firstname"],
    deliveryDate: json["delivery_date"] == null ? null : DateTime.parse(json["delivery_date"]),
    deliveryTime: json["delivery_time"] == null ? null : json["delivery_time"],
    shippingAddress: json["shipping_address"] == null ? null : json["shipping_address"],
    vegCount: json["vegCount"] == null ? null : json["vegCount"],
    groCount: json["groCount"] == null ? null : json["groCount"],
    retrivalCount: json["retrivalCount"] == null ? null : json["retrivalCount"],
    pendingCount: json["pendingCount"] == null ? null : json["pendingCount"],
    notFoundCount: json["not_found_count"] == null ? null : json["not_found_count"],
    lockStatus: json["lock_status"] == null ? null : json["lock_status"],
    retrieveStatus: json["retrieve_status"] == null ? null : json["retrieve_status"],
    priorityOrder: json["priority_order"] == null ? null : json["priority_order"],
  );

  Map<String, dynamic> toMap() => {
    "order_id": orderId == null ? null : orderId,
    "item_count": itemCount == null ? null : itemCount,
    "firstname": firstname == null ? null : firstname,
    "delivery_date": deliveryDate == null ? null : "${deliveryDate.year.toString().padLeft(4, '0')}-${deliveryDate.month.toString().padLeft(2, '0')}-${deliveryDate.day.toString().padLeft(2, '0')}",
    "delivery_time": deliveryTime == null ? null : deliveryTime,
    "shipping_address": shippingAddress == null ? null : shippingAddress,
    "vegCount": vegCount == null ? null : vegCount,
    "groCount": groCount == null ? null : groCount,
    "retrivalCount": retrivalCount == null ? null : retrivalCount,
    "pendingCount": pendingCount == null ? null : pendingCount,
    "not_found_count": notFoundCount == null ? null : notFoundCount,
    "lock_status": lockStatus == null ? null : lockStatus,
    "retrieve_status": retrieveStatus == null ? null : retrieveStatus,
    "priority_order": priorityOrder == null ? null : priorityOrder,
  };
}

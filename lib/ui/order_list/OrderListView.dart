

import 'package:retrivalapp/ui/order_list/OrderListModel.dart';

abstract class OrderListView {
  void onFailed(String errorMessage);
  void populateOrderList(List<OrderListModel> list,int currentTab);
}
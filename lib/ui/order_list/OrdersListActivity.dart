import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:retrivalapp/ui/order_details/OrderDetailsNew/OrderDeatilHome.dart';
import 'package:retrivalapp/ui/order_details/database/DatabaseHelper.dart';
import 'package:retrivalapp/ui/order_list/OrderListModel.dart';
import 'package:retrivalapp/ui/order_list/OrderListPresenter.dart';
import 'package:retrivalapp/ui/order_list/OrderListView.dart';
import 'package:retrivalapp/utils/AppColor.dart';
import 'package:retrivalapp/utils/Constants.dart';
import 'package:retrivalapp/utils/Dialogs.dart';
import 'package:retrivalapp/utils/Utility.dart';
import 'package:toast/toast.dart';

class OrdersListActivity extends StatefulWidget
{
  @override
  OrderListScreen createState() => OrderListScreen();
}

class OrderListScreen extends State<OrdersListActivity> with SingleTickerProviderStateMixin implements OrderListView
{
  final String TAG = "OrdersListActivity";
  OrderListPresenter presenter;
  TabController controller;
  int currentTab = 0;
  Text selectedStoreName;
  String selectedDate = "";
  Widget appBarTitle;
  List<OrderListModel> searchDataList = new List();
  DateTime selectedDateTime = DateTime.now();
  DateTime todayDate = DateTime.now();
  final TextEditingController _searchController = new TextEditingController();
  List<OrderListModel> pendingDataList = new List();
  List<OrderListModel> notFoundDataList = new List();
  List<OrderListModel> retrievedDataList = new List();


  OrderListScreen() {
    presenter = OrderListPresenter(this);
  }

  @override
  Widget build(BuildContext context)
  {
    return new DefaultTabController(
      length: 3,
      child: new Scaffold(
        appBar: new AppBar(
          backgroundColor: colorPrimary,
          actions: [

            InkWell(
              child: Icon(
                Icons.history,
                color: Colors.white,
              ),
              onTap: () {
                onRefreshClick();
              },
            )
          ],
          title: appBarTitle,
          bottom: new TabBar(
            controller: controller,
            onTap: handleTabSelection,
            tabs: [
              Tab(
                icon: new Icon(Icons.hourglass_empty),
                text: "Pending",
              ),
              Tab(
                icon: new Icon(Icons.add_shopping_cart),
                text: "Retrieved",
              ),
              Tab(
                icon: new Icon(Icons.mood_bad),
                text: "Not Found",
              ),
            ],
          ),
        ),
        body: new TabBarView(
          controller: controller,
          children: <Widget>[
            _pendingOrderList(),
            _retrievedOrderList(),
            _buildNotFoundList()
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  void initState() {

    controller = TabController(
      length: 3,
      vsync: this,
    );

    controller.addListener(handleTabViewSelection);

    super.initState();

    if (selectedDate.isEmpty)
    {

      var now = new DateTime.now();
      var formatter = new DateFormat('dd-MM-yyyy');
      selectedDate = formatter.format(now);
      var apiFormat = new DateFormat('yyyy-MM-dd');
      Utility.setStringPreference(Constants.ORDER_DATE, apiFormat.format(now));

    }

    setAppBarTitle(context);
    loadOrderList(currentTab,"118");
  }

  void setAppBarTitle(BuildContext context) async
  {
    String storeName = await Utility.getStringPreference(Constants.PRIMARY_STORE_NAME);
    if (storeName.isEmpty) {
      storeName = await Utility.getStringPreference(Constants.STORE_NAME);
    }
    selectedStoreName = new Text(storeName, style: new TextStyle(color: Colors.white, fontSize: 18),);
    appBarTitle = GestureDetector(
      onTap: () {},
      child: new Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Center(child: Container(child: selectedStoreName)),
            Padding(
                padding: EdgeInsets.fromLTRB(0, 5, 0, 0),
                child: Text("(" + selectedDate + ")",
                    style: new TextStyle(color: Colors.white, fontSize: 14))),
          ]),
    );
    setState(() {});
  }

  void updateSelectedDate(String date) {
    setState(() {
      selectedDate = date;
      setAppBarTitle(context);
    });
  }

  void handleTabSelection(int tabIndex) {
      setState(() {
        currentTab = controller.index;
      });
      loadOrderList(currentTab,"158");

  }
  void handleTabViewSelection() {
      setState(() {
        currentTab = controller.index;
      });

  }

  void filterSearchResults(String query) {
    List<OrderListModel> dummySearchList = List();
    dummySearchList.addAll(pendingDataList);
    if (query.isNotEmpty) {
      print("Query : " + query);
      List<OrderListModel> dummyListData = List();
      dummySearchList.forEach((item) {
        if ((item.orderId.toString()).contains(query)) {
          dummyListData.add(item);
        }
      });
      setState(() {
        searchDataList.clear();
        searchDataList.addAll(dummyListData);
      });
      return;
    } else {
      print("Query is empty: " + query);
      setState(() {
        searchDataList.clear();
        searchDataList.addAll(pendingDataList);
      });
    }
  }

  Future<Null> selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDateTime,
        firstDate: todayDate.subtract(new Duration(days: 4)),
        lastDate: todayDate.add(new Duration(days: 2)));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDateTime = picked;
        /**
         * set date format for api
         */
        var apiFormat = new DateFormat('yyyy-MM-dd');
        Utility.setStringPreference(Constants.ORDER_DATE, apiFormat.format(picked));

        var formatter = new DateFormat('dd-MM-yyyy');
        String ddmmyyyy = formatter.format(picked);
        updateSelectedDate(ddmmyyyy);

        loadOrderList(currentTab,"215");

      });
  }

  void loadOrderList(int currentTab,String linenumber) async {
    Dialogs.showProgressDialog(await context);
//    print("=================================================>Line number: "+linenumber);
    presenter.getOrderList(currentTab);
  }

  // Pending Order List
  Widget _pendingOrderList() {
    return Column(
      children: <Widget>[
        Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              new Flexible(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(5, 10, 10, 5),
                  child: TextField(
                    onChanged: (value) {
                      filterSearchResults(value);
                    },
                    controller: _searchController,
                    decoration: InputDecoration(
                        labelText: "Search",
                        hintText: "Search order by order id",
                        prefixIcon: Icon(Icons.search),
                        border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(5.0)))),
                    style: TextStyle(
                        fontSize: 16, height: 1.0, color: Colors.black),
                  ),
                ),
                flex: 9,
              ),
              new Flexible(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(0, 0, 5, 0),
                  child: IconButton(
                    icon: Icon(Icons.calendar_today),
                    onPressed: () {
                      selectDate(context);
                    },
                  ),
                ),
                flex: 1,
              ),
            ]),
        (searchDataList.length != null && searchDataList.length != 0)
            ? Expanded(
                child: ListView.builder(
                    shrinkWrap: true,
                    padding: EdgeInsets.all(8),
                    itemCount: searchDataList.length,
                    itemBuilder: (BuildContext context, int index) {
                      return new GestureDetector(
                        onTap: () {
                          onRowClick(searchDataList[index]);
                        },
                        child: Card(
                          color: Colors.white,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: Column(
                                    // align the text to the left instead of centered
                                    crossAxisAlignment:CrossAxisAlignment.start,
                                    children: <Widget>[
                                      searchDataList[index].priorityOrder.isEmpty
                                          ? new Container()
                                          : Text("Priority Order",
                                                  style: TextStyle(
                                                  color: Colors.red,
                                                  fontSize: 12),
                                            ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          new Flexible(
                                            child: Padding(
                                              padding: EdgeInsets.fromLTRB(
                                                  0, 0, 0, 0),
                                              child: Text(
                                                searchDataList[index].orderId,
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 16,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ),
                                            flex: 1,
                                          ),

                                          new Flexible(
                                            child: Padding(
                                              padding: EdgeInsets.fromLTRB(
                                                  0, 0, 0, 0),
                                              child: Text(
                                                  searchDataList[index]
                                                      .firstname,
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 16),
                                                  textAlign: TextAlign.center),
                                            ),
                                            flex: 1,
                                          ),
                                          new Flexible(
                                            child: Padding(
                                              padding: EdgeInsets.fromLTRB(
                                                  0, 0, 0, 0),
                                              child: Text(
                                                setDeliveryDateAndTine(
                                                    searchDataList[index]),
                                                style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 12,
                                                ),
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                            flex: 1,
                                          ),
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          new Flexible(
                                            child: Padding(
                                              padding: EdgeInsets.fromLTRB(
                                                  0, 10, 0, 0),
                                              child: Text(
                                                  "TI : " +
                                                      searchDataList[index]
                                                          .itemCount
                                                          .toString(),
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 16),
                                                  textAlign: TextAlign.center),
                                            ),
                                            flex: 1,
                                          ),
                                          new Flexible(
                                            child: Padding(
                                              padding: EdgeInsets.fromLTRB(
                                                  0, 10, 0, 0),
                                              child: Text(
                                                  "PI : " +
                                                      searchDataList[index]
                                                          .pendingCount
                                                          .toString(),
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 16),
                                                  textAlign: TextAlign.center),
                                            ),
                                            flex: 1,
                                          ),
                                          new Flexible(
                                            child: Padding(
                                              padding: EdgeInsets.fromLTRB(
                                                  0, 10, 0, 0),
                                              child: Text(
                                                "RI : " +
                                                    searchDataList[index]
                                                        .retrivalCount
                                                        .toString(),
                                                style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 16,
                                                ),
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                            flex: 1,
                                          ),
                                          new Flexible(
                                            child: Padding(
                                              padding: EdgeInsets.fromLTRB(
                                                  0, 10, 0, 0),
                                              child: Text(
                                                "NF : " +
                                                    searchDataList[index]
                                                        .notFoundCount
                                                        .toString(),
                                                style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 16,
                                                ),
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                            flex: 1,
                                          ),
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          new Flexible(
                                            child: Padding(
                                              padding: EdgeInsets.fromLTRB(
                                                  0, 10, 0, 0),
                                              child: Text(
                                                  "Vegetable Count : " +
                                                      searchDataList[index]
                                                          .vegCount
                                                          .toString(),
                                                  style: TextStyle(
                                                      color: Colors.green,
                                                      fontSize: 16),
                                                  textAlign: TextAlign.center),
                                            ),
                                            flex: 1,
                                          ),
                                          new Flexible(
                                            child: Padding(
                                              padding: EdgeInsets.fromLTRB(
                                                  0, 10, 0, 0),
                                              child: Text(
                                                  "Grocery Count : " +
                                                      searchDataList[index]
                                                          .groCount
                                                          .toString(),
                                                  style: TextStyle(
                                                      color: Colors.blue,
                                                      fontSize: 16),
                                                  textAlign: TextAlign.center),
                                            ),
                                            flex: 1,
                                          ),
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          new Flexible(
                                            child: Padding(
                                              padding: EdgeInsets.fromLTRB(
                                                  0, 10, 0, 0),
                                              child: Text(
                                                  searchDataList[index]
                                                      .shippingAddress,
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 16)),
                                            ),
                                            flex: 1,
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    }),
              )
            : Center(child: Text("opps! no pending order")),
      ],
    );
  }

  Widget _retrievedOrderList() {
    return (retrievedDataList.length != null && retrievedDataList.length != 0)
        ? ListView.builder(
        padding: EdgeInsets.all(8),
        itemCount: retrievedDataList.length,
        itemBuilder: (BuildContext context, int index) {
          return new GestureDetector(
            onTap: () {
              onRowClick(retrievedDataList[index]);
            },
            child: Card(
              color: Colors.black,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Column(
                        // align the text to the left instead of centered
                        crossAxisAlignment: CrossAxisAlignment.start,

                        children: <Widget>[
                          retrievedDataList[index].priorityOrder.isEmpty
                              ? new Container()
                              : Text(
                            "Priority Order",
                            style: TextStyle(
                                color: Colors.red, fontSize: 12),
                          ),
                          Row(
                            mainAxisAlignment:
                            MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              new Flexible(
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                  child: Text(
                                    retrievedDataList[index].orderId,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                flex: 1,
                              ),
                              new Flexible(
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                  child: Text(retrievedDataList[index].firstname,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 16),
                                      textAlign: TextAlign.center),
                                ),
                                flex: 1,
                              ),
                              new Flexible(
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                  child: Text(
                                    setDeliveryDateAndTine(retrievedDataList[index]),
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 12,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                flex: 1,
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment:
                            MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              new Flexible(
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                  child: Text(
                                      "TI : " +
                                          retrievedDataList[index]
                                              .itemCount
                                              .toString(),
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 16),
                                      textAlign: TextAlign.center),
                                ),
                                flex: 1,
                              ),
                              new Flexible(
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                  child: Text(
                                      "PI : " +
                                          retrievedDataList[index]
                                              .pendingCount
                                              .toString(),
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 16),
                                      textAlign: TextAlign.center),
                                ),
                                flex: 1,
                              ),
                              new Flexible(
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                  child: Text(
                                    "RI : " +
                                        retrievedDataList[index]
                                            .retrivalCount
                                            .toString(),
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 16,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                flex: 1,
                              ),
                              new Flexible(
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                  child: Text(
                                    "NF : " +
                                        retrievedDataList[index]
                                            .notFoundCount
                                            .toString(),
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 16,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                flex: 1,
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment:
                            MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              new Flexible(
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                  child: Text(
                                      "Vegetable Count : " +
                                          retrievedDataList[index]
                                              .vegCount
                                              .toString(),
                                      style: TextStyle(
                                          color: Colors.green,
                                          fontSize: 16),
                                      textAlign: TextAlign.center),
                                ),
                                flex: 1,
                              ),
                              new Flexible(
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                  child: Text(
                                      "Grocery Count : " +
                                          retrievedDataList[index]
                                              .groCount
                                              .toString(),
                                      style: TextStyle(
                                          color: Colors.blue, fontSize: 16),
                                      textAlign: TextAlign.center),
                                ),
                                flex: 1,
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment:
                            MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              new Flexible(
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                  child: Text(
                                      retrievedDataList[index].shippingAddress,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 16)),
                                ),
                                flex: 1,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        })
        : Center(child: Text("opps! no pending order"));
  }

  Widget _buildNotFoundList() {
    return (notFoundDataList.length != null && notFoundDataList.length != 0)
        ? ListView.builder(
            padding: EdgeInsets.all(8),
            itemCount: notFoundDataList.length,
            itemBuilder: (BuildContext context, int index) {
              return new GestureDetector(
                onTap: () {
                  onRowClick(notFoundDataList[index]);
                },
                child: Card(
                  color: Colors.black12,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Column(
                            // align the text to the left instead of centered
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              notFoundDataList[index].priorityOrder.isEmpty
                                  ? new Container()
                                  : Text(
                                      "Priority Order",
                                      style: TextStyle(
                                          color: Colors.red, fontSize: 12),
                                    ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  new Flexible(
                                    child: Padding(
                                      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                      child: Text(
                                        notFoundDataList[index].orderId,
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    flex: 1,
                                  ),
                                  new Flexible(
                                    child: Padding(
                                      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                      child: Text(notFoundDataList[index].firstname,
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 16),
                                          textAlign: TextAlign.center),
                                    ),
                                    flex: 1,
                                  ),
                                  new Flexible(
                                    child: Padding(
                                      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                      child: Text(
                                        setDeliveryDateAndTine(notFoundDataList[index]),
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 12,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    flex: 1,
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  new Flexible(
                                    child: Padding(
                                      padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                      child: Text(
                                          "TI : " +
                                              notFoundDataList[index]
                                                  .itemCount
                                                  .toString(),
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 16),
                                          textAlign: TextAlign.center),
                                    ),
                                    flex: 1,
                                  ),
                                  new Flexible(
                                    child: Padding(
                                      padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                      child: Text(
                                          "PI : " +
                                              notFoundDataList[index]
                                                  .pendingCount
                                                  .toString(),
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 16),
                                          textAlign: TextAlign.center),
                                    ),
                                    flex: 1,
                                  ),
                                  new Flexible(
                                    child: Padding(
                                      padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                      child: Text(
                                        "RI : " +
                                            notFoundDataList[index]
                                                .retrivalCount
                                                .toString(),
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 16,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    flex: 1,
                                  ),
                                  new Flexible(
                                    child: Padding(
                                      padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                      child: Text(
                                        "NF : " +
                                            notFoundDataList[index]
                                                .notFoundCount
                                                .toString(),
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 16,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    flex: 1,
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  new Flexible(
                                    child: Padding(
                                      padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                      child: Text(
                                          "Vegetable Count : " +
                                              notFoundDataList[index]
                                                  .vegCount
                                                  .toString(),
                                          style: TextStyle(
                                              color: Colors.green,
                                              fontSize: 16),
                                          textAlign: TextAlign.center),
                                    ),
                                    flex: 1,
                                  ),
                                  new Flexible(
                                    child: Padding(
                                      padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                      child: Text(
                                          "Grocery Count : " +
                                              notFoundDataList[index]
                                                  .groCount
                                                  .toString(),
                                          style: TextStyle(
                                              color: Colors.blue, fontSize: 16),
                                          textAlign: TextAlign.center),
                                    ),
                                    flex: 1,
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  new Flexible(
                                    child: Padding(
                                      padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                      child: Text(
                                          notFoundDataList[index].shippingAddress,
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 16)),
                                    ),
                                    flex: 1,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            })
        : Center(child: Text("opps! no pending order"));
  }

  void onRowClick(OrderListModel model)
  {
    if(currentTab == 0)
    {
      Utility.setStringPreference(Constants.ORDER_ID, model.orderId);
      if (model.lockStatus == 0) {
        Utility.setBooleanPreference(Constants.IS_ORDER_LOCK, false);
        /**
         * delete product from retrieval product if exist
         */
      } else {
        Utility.setBooleanPreference(Constants.IS_ORDER_LOCK, true);
      }


      /**
       * Clear both product table
       */
      String delivery_date=model.deliveryDate.toString();
      String delivery_time=model.deliveryTime.toString();
      String Lock=model.lockStatus.toString();
      Navigator.push(context, MaterialPageRoute(builder: (context) => OrderDetailHome(delivery_date,delivery_time)));
    }

  }

  void onRefreshClick() {
    loadOrderList(currentTab,"917");
  }

  String setDeliveryDateAndTine(OrderListModel model) {

    /**
     * get month
     */

    String month = model.deliveryDate.month.toString();
    if (month.length < 2) {
      month = "0" + month;
    }

    /**
     * get date
     */

    String day = model.deliveryDate.day.toString();
    if (day.length < 2) {
      day = "0" + day;
    }

    return (day + "-" + month + "\n" + model.deliveryTime);
  }

  @override
  void onFailed(String errorMessage) {
    Dialogs.hideProgressDialog(context);
    Toast.show(errorMessage, context,duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
  }

  @override
  void populateOrderList(List<OrderListModel> data,int currentTab) {
    setState(() {
      Dialogs.hideProgressDialog(context);
//      isAPICalled = true;
      switch(currentTab){
        case 0:
          {
            pendingDataList.clear();
            this.pendingDataList = data;
            searchDataList.clear();
            this.searchDataList.addAll(pendingDataList);
          }
          break;

        case 1:
          {
            retrievedDataList.clear();
            this.retrievedDataList = data;
          }

          break;

        case 2:
          {
            notFoundDataList.clear();
            this.notFoundDataList = data;
          }
          break;
      }

    });
  }
}

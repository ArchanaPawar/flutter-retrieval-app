import 'package:retrivalapp/ui/order_list/OrderListModel.dart';
import 'package:retrivalapp/ui/order_list/OrderListView.dart';
import 'package:retrivalapp/utils/APIConstant.dart';
import 'package:retrivalapp/utils/ApiController.dart';
import 'package:retrivalapp/utils/Constants.dart';
import 'package:retrivalapp/utils/Message.dart';
import 'package:retrivalapp/utils/NetworkCheck.dart';
import 'package:retrivalapp/utils/Utility.dart';
import 'OrderListResponse.dart';

class OrderListPresenter
{
  final String TAG = "OrderListPresenter";
  OrderListView view;
  OrderListPresenter(this.view);
  ApiController apiController = new ApiController.internal();

  void getOrderList(int currentTab) async {
    if (await NetworkCheck.check())
    {
      var xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
          "<order_info>\n " +
          "<store_id>" +
          (await Utility.getStringPreference(Constants.STORE_CODE)) +
          "</store_id>\n" +
          "<user_id>" +
          (await Utility.getStringPreference(Constants.EMPLOYEE_ID)) +
          "</user_id>\n" +
          "<primary_store_id>" +
          (await Utility.getStringPreference(Constants.PRIMARY_STORE_ID)) +
          "</primary_store_id>\n " +
          "<order_date>" +
          (await Utility.getStringPreference(Constants.ORDER_DATE)) +
          "</order_date>\n" +
          "</order_info>";

      apiController
          .postXmlData(APIConstant.ordersList, xml)
          .then((String apiResponse) {
        Utility.log(TAG, "Response: " + apiResponse);
        OrderListResponse response = OrderListResponse.fromJson(apiResponse);
        if (response.status) {
          List<OrderListModel> list = new List();
          switch (currentTab) {
            case 0:
              {
                for (var i = 0; i < response.data.length; i++) {
                  if (response.data[i].retrieveStatus == 0) {
                    list.add(new OrderListModel(
                        response.data[i].orderId,
                        response.data[i].itemCount,
                        response.data[i].firstname,
                        response.data[i].deliveryDate,
                        response.data[i].deliveryTime,
                        response.data[i].shippingAddress,
                        response.data[i].vegCount,
                        response.data[i].groCount,
                        response.data[i].retrivalCount,
                        response.data[i].pendingCount,
                        response.data[i].notFoundCount,
                        response.data[i].lockStatus,
                        response.data[i].retrieveStatus,
                        response.data[i].priorityOrder)
                    );
                  }
                }
              }
              break;

            case 1:
              {
                for (var i = 0; i < response.data.length; i++) {
                  if (response.data[i].retrieveStatus == 1 &&
                      response.data[i].lockStatus == 1 &&
                      int.parse(response.data[i].notFoundCount) == 0) {
//                  if (response.data[i].retrieveStatus == 0) {
                    list.add(new OrderListModel(
                        response.data[i].orderId,
                        response.data[i].itemCount,
                        response.data[i].firstname,
                        response.data[i].deliveryDate,
                        response.data[i].deliveryTime,
                        response.data[i].shippingAddress,
                        response.data[i].vegCount,
                        response.data[i].groCount,
                        response.data[i].retrivalCount,
                        response.data[i].pendingCount,
                        response.data[i].notFoundCount,
                        response.data[i].lockStatus,
                        response.data[i].retrieveStatus,
                        response.data[i].priorityOrder));
                  }
                }
                print(TAG+list.length.toString());
              }
              break;


          case 2:
              {
                for (var i = 0; i < response.data.length; i++) {
                  if (response.data[i].retrieveStatus == 1 &&
                      response.data[i].lockStatus == 1 &&
                      int.parse(response.data[i].notFoundCount) > 0) {
//                  if (response.data[i].retrieveStatus == 0) {
                    list.add(new OrderListModel(
                        response.data[i].orderId,
                        response.data[i].itemCount,
                        response.data[i].firstname,
                        response.data[i].deliveryDate,
                        response.data[i].deliveryTime,
                        response.data[i].shippingAddress,
                        response.data[i].vegCount,
                        response.data[i].groCount,
                        response.data[i].retrivalCount,
                        response.data[i].pendingCount,
                        response.data[i].notFoundCount,
                        response.data[i].lockStatus,
                        response.data[i].retrieveStatus,
                        response.data[i].priorityOrder));
                  }
                }
                print(TAG+list.length.toString());
              }
              break;
          }

          view.populateOrderList(list, currentTab);
        } else {
          view.onFailed(response.message);
        }
      }).catchError((error) {
        Utility.log(TAG, error.toString());
        view.onFailed(Message.SOMETHING_WENT_WRONG);
      });
    } else {
      view.onFailed(Message.NO_INTERNET);
    }
  }
}


import 'package:retrivalapp/ui/home/HomeView.dart';
import 'package:retrivalapp/utils/APIConstant.dart';
import 'package:retrivalapp/utils/ApiController.dart';
import 'package:retrivalapp/utils/Constants.dart';
import 'package:retrivalapp/utils/Message.dart';
import 'package:retrivalapp/utils/NetworkCheck.dart';
import 'package:retrivalapp/utils/Utility.dart';

import 'RetrievalMenuResponse.dart';

class HomePresenter {
  final String TAG = "HomePresenter";
  HomeView view;
  HomePresenter(this.view);
  ApiController apiController = new ApiController.internal();

  void getRetrievalMenu() async {
    if (await NetworkCheck.check()) {
      var xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
          "<user_info>\n " +
          "<user_id>" + await Utility.getStringPreference(Constants.EMPLOYEE_ID) + "</user_id>\n" +
          "<store_id>" + await Utility.getStringPreference(Constants.STORE_ID) + "</store_id>\n" +
          "</user_info>";
      apiController.postXmlDataWithHeader(APIConstant.getRetrievalMenu, xml).then((String apiResponse) {
        Utility.log(TAG, "Response: " + apiResponse);
        RetrievalMenuResponse response = RetrievalMenuResponse.fromJson(apiResponse);
        if (response.status) {
          view.populateMenuList(response.data);
        } else {
          view.onFailed(response.message);
        }
      }).catchError((error) {
        Utility.log(TAG, error.toString());
        view.onFailed(Message.SOMETHING_WENT_WRONG);
      });
    } else {
      view.onFailed(Message.NO_INTERNET);
    }

  }

}
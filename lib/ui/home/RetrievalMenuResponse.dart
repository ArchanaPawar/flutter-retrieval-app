// To parse this JSON data, do
//
//     final retrievalMenuResponse = retrievalMenuResponseFromMap(jsonString);

import 'dart:convert';

class RetrievalMenuResponse {
  RetrievalMenuResponse({
    this.status,
    this.message,
    this.data,
  });

  bool status;
  String message;
  List<Datum> data;

  factory RetrievalMenuResponse.fromJson(String str) => RetrievalMenuResponse.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory RetrievalMenuResponse.fromMap(Map<String, dynamic> json) => RetrievalMenuResponse(
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
    data: json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromMap(x))),
  );

  Map<String, dynamic> toMap() => {
    "status": status == null ? null : status,
    "message": message == null ? null : message,
    "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toMap())),
  };
}

class Datum {
  Datum({
    this.code,
    this.name,
    this.isActive,
  });

  String code;
  String name;
  bool isActive;

  factory Datum.fromJson(String str) => Datum.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Datum.fromMap(Map<String, dynamic> json) => Datum(
    code: json["code"] == null ? null : json["code"],
    name: json["name"] == null ? null : json["name"],
    isActive: json["is_active"] == null ? null : json["is_active"],
  );

  Map<String, dynamic> toMap() => {
    "code": code == null ? null : code,
    "name": name == null ? null : name,
    "is_active": isActive == null ? null : isActive,
  };
}

import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:retrivalapp/ui/PendingOrder/PendingListActivity.dart';
import 'package:retrivalapp/ui/home/HomePresenter.dart';
import 'package:retrivalapp/ui/home/HomeView.dart';
import 'package:retrivalapp/ui/home/RetrievalMenuResponse.dart';
import 'package:retrivalapp/ui/order_list/OrdersListActivity.dart';
import 'package:retrivalapp/ui/select_store/SelectStoreActivity.dart';
import 'package:retrivalapp/ui/splash/SplashActivity.dart';
import 'package:retrivalapp/utils/AppColor.dart';
import 'package:retrivalapp/utils/Constants.dart';
import 'package:retrivalapp/utils/Utility.dart';
import 'package:retrivalapp/utils/appKeys.dart';
import 'package:toast/toast.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';
class HomeActivity extends StatefulWidget
{
  @override
  HomeScreen createState() => HomeScreen();
}

class HomeScreen extends State<HomeActivity> implements HomeView {
  final String TAG = "HomeActivity";
  HomePresenter presenter;
  Text loggedInUserName;
  Text selectedStore;
  List<Datum> dataList;

  Widget appBarTitle;
  Icon actionIcon = new Icon(Icons.exit_to_app, color: Colors.white,);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: DoubleBackToCloseApp(
        child: Scaffold(
          appBar: buildBar(context),
          body: dataList == null? Center(child: Utility.buildProgressIndicator(true),): ListView(
            padding: new EdgeInsets.symmetric(vertical: 8.0),
            children:  retrievalOptionMenu(),
          ),
        ),
        snackBar: const SnackBar(
          content: Text('Tap back again to leave'),
        ),
      ),
    );
  }

  List<RetrievalMenu> retrievalOptionMenu() {
    return dataList.map((Datum) => new RetrievalMenu(Datum)).toList();
  }

  @override
  void initState() {
    super.initState();
    presenter = HomePresenter(this);
    presenter.getRetrievalMenu();
    setAppBarTitle(context);
  }

  Widget buildBar(BuildContext context) {
    return new AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: colorPrimary,
        title: appBarTitle,
        actions: <Widget>[
          new IconButton(icon: actionIcon, onPressed: () {
            logoutFromApplication();
          },),
        ]
    );

  }

  void setAppBarTitle(BuildContext context) async {
    loggedInUserName = new Text(await Utility.getStringPreference(Constants.NAME), style: new TextStyle(color: Colors.white,fontSize: 20),);
    selectedStore = new Text("("+await Utility.getStringPreference(Constants.STORE_NAME)+((await Utility.getStringPreference(Constants.PRIMARY_STORE_NAME)).isEmpty?"":"->"+await Utility.getStringPreference(Constants.PRIMARY_STORE_NAME))+")", maxLines:2, style: new TextStyle(color: Colors.white,fontSize: 15),);
    appBarTitle = GestureDetector(
      onTap: (){
        Navigator.of(context).pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => SelectStoreActivity()));
        },
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          loggedInUserName,selectedStore
        ],
      ),
    );
    setState(() {

    });
  }

  void logoutFromApplication(){
    /**
     * yes button
     */
      Widget yesButton = FlatButton(
        child: Text("Yes"),
        onPressed: () {
          Utility.clearSharedPreference(context);
          Navigator.of(context).pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
          Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => SplashActivity()));
        },
      );

      /**
       * Not now button
       */
      Widget noButton = FlatButton(
        child: Text("Not now"),
        onPressed: () {
          Navigator.of(context).pop();
        },
      );

      /**
       * set up the AlertDialog
       */
      AlertDialog alert = AlertDialog(
        content: Text("Are sure, You want to logout?"),
        actions: [
          noButton,
          yesButton,
        ],
      );

      /**
       * show the dialog
       */
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      );
  }

  @override
  void onFailed(String errorMessage) {
    Toast.show(errorMessage, context,
        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
  }

  @override
  void populateMenuList(List<Datum> data) {
  setState(() {
    dataList = data;
  });
  }
}

class RetrievalMenu extends StatelessWidget {
  TextStyle style = TextStyle(
      color: Colors.white, fontFamily: 'Montserrat', fontSize: 18.0,fontWeight: FontWeight.normal);
  final Datum _datum;

  RetrievalMenu(this._datum);

  @override
  Widget build(BuildContext context) {
    return _datum.isActive?GestureDetector(
      child:Container(
        width: screenWidth(context),
        height: 38,
        margin: const EdgeInsets.only( left: 12,right: 12,top: 12),
        child:MaterialButton(
          minWidth:screenWidth(context),
          onPressed: (){
            onRowClick(context);
          },
          color: menuButtonColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(20))
          ),
          child:  Text(
            _datum.name,
            maxLines: 2,
            style: style,
          ),
        )
      ),
    ):Container();
  }

  void onRowClick(BuildContext context) {
    /**
     * Order retrieval
     */
    if (Utility.equalsIgnoreCase("RT", _datum.code))
    {
      Utility.setStringPreference(Constants.ORDER_DATE, "");
      Navigator.push(context, MaterialPageRoute(builder: (context) => OrdersListActivity()));
      /**
       * Government Order List
       */
    }else if(Utility.equalsIgnoreCase("GOL",_datum.code)){
      Toast.show(_datum.name+"(Work latter on it)", context, duration: Toast.LENGTH_LONG,
          gravity: Toast.BOTTOM);
      /**
       * Pending Stock
       */
    }else if(Utility.equalsIgnoreCase("PS",_datum.code)){
      /*
      Toast.show(_datum.name+"(Work latter on it)", context, duration: Toast.LENGTH_LONG,
          gravity: Toast.BOTTOM);
          */
      Utility.setStringPreference(Constants.ORDER_DATE, "");
      Navigator.push(context, MaterialPageRoute(builder: (context) => StoreStockActivity()));
    }else{
      Toast.show("Invalid selection: "+_datum.name+"("+_datum.code+")", context, duration: Toast.LENGTH_LONG,
          gravity: Toast.BOTTOM);
    }
  }
}
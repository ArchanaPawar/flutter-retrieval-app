
import 'package:retrivalapp/ui/home/RetrievalMenuResponse.dart';

abstract class HomeView {
  void onFailed(String errorMessage);
  void populateMenuList(List<Datum> data);
}
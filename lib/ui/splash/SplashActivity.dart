import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get_version/get_version.dart';
import 'package:retrivalapp/ui/home/HomeActivity.dart';
import 'package:retrivalapp/ui/login/LoginActivity.dart';
import 'package:retrivalapp/ui/select_store/SelectStoreActivity.dart';
import 'package:retrivalapp/ui/splash/SplashPresenter.dart';
import 'package:retrivalapp/ui/splash/SplashView.dart';
import 'package:retrivalapp/utils/Constants.dart';
import 'package:retrivalapp/utils/Dialogs.dart';
import 'package:retrivalapp/utils/Message.dart';
import 'package:retrivalapp/utils/NetworkCheck.dart';
import 'package:retrivalapp/utils/Resources.dart';
import 'package:retrivalapp/utils/Utility.dart';
import 'package:retrivalapp/utils/appKeys.dart';
import 'package:toast/toast.dart';

class SplashActivity extends StatefulWidget {
  @override
  State createState() => SplashScreen();
}

class SplashScreen extends State<SplashActivity>
    implements SplashView, ClickInterface {
  final String TAG = "SplashActivity";
  String appVersion = '';
  String versionCode = '';
  bool isConnected = false;
  SplashPresenter presenter;

  @override
  void initState() {
    super.initState();
    presenter = SplashPresenter(this);
    checkConnection();
  //  continueExecution();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: screenWidth(context),
        height: screenHeight(context),
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(Resources.splash_bg), fit: BoxFit.fill)),
        alignment: Alignment.center,
        child: Stack(
          children: [
            Center(
              child: Image(
                image: AssetImage(Resources.splash_logo),
                width: 120,
                height: 120,
              ),
            ),
            Positioned(
              bottom: 10,
              child: isConnected
                  ? Container()
                  : Container(
                      width: screenWidth(context),
                      alignment: Alignment.center,
                      child: InkWell(
                        onTap: () {
                          checkConnection();
                        },
                        child: Text(
                          "Refresh",
                          style: TextStyle(color: Colors.white, fontSize: 15),
                        ),
                      ),
                    ),
            )
          ],
        ),
      ),
    );
  }

  void checkConnection() async {
    if (await NetworkCheck.check()) {
      setState(() {
        isConnected = true;
        /**
         * get application version according to os
         */
        initPlatformState();
      });
    } else {
      setState(() {
        isConnected = false;
      });
      Toast.show(Message.NO_INTERNET, context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    }
  }

  void initPlatformState() async {
    /**
     * get device info
     */
    String deviceInfo;
    try {
      deviceInfo = await GetVersion.platformVersion;
      Utility.log(TAG, "deviceInfo: " + deviceInfo);
    }catch(e){

      deviceInfo = 'Failed to get platform version.';
    }

    /**
     * projectVersion
     */


    String projectVersion;
    try {
      projectVersion = await GetVersion.projectVersion;
      Utility.log(TAG, "projectVersion: " + projectVersion);
    } on PlatformException {
      projectVersion = 'Failed to get project version.';
    }



    /**
     *
     */
    String projectCode;
    try {
      projectCode = await GetVersion.projectCode;
      Utility.log(TAG, "projectCode: " + projectCode);
    } on PlatformException {
      projectCode = 'Failed to get build number.';
    }

    /**
     *
     */
    String projectAppID;
    try {
      projectAppID = await GetVersion.appID;
      Utility.log(TAG, "projectAppID: " + projectAppID);
    } on PlatformException {
      projectAppID = 'Failed to get app ID.';
    }

    /**
     *
     */
    String projectName;
    try
    {
      projectName = await GetVersion.appName;
      Utility.log(TAG, "projectName: " + projectName.toString());
    } catch(e) {
      projectName = 'Failed to get app name.';
    }

    if (!mounted) return;

    setState(() {
      appVersion = projectVersion;
      versionCode = projectCode;
    });

    presenter.validateAppVersion(versionCode, appVersion);
  }

  @override
  void onFailed(String errorMessage) {
    Toast.show(errorMessage, context,
        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
  }

  @override
  void onHomeActivity() {
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeActivity()));
  }

  @override
  void onSelectStoreActivity() {
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => SelectStoreActivity()));


  }

  @override
  void onStartLoginActivity() {
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => LoginActivity()));
  }

  @override
  void updateApp(bool isMandatory, String message) {
    if (isMandatory) {
      Dialogs.showMendetoryUpdate(context, message);
    } else {
      Dialogs.showNonMendetoryUpdate(context, message, this);
    }
  }

  @override
  void onClick() {
    continueExecution();
  }

  @override
  continueExecution() async {
    bool isLoggedIn = await Utility.getBooleanPreference(Constants.IS_LOGIN);
    var duration = Duration(seconds: 2);
    if (isLoggedIn) {
      String storeId = await Utility.getStringPreference(Constants.STORE_ID);
      if (storeId.isEmpty) {
        return new Timer(duration, onSelectStoreActivity);
      } else {
        return new Timer(duration, onHomeActivity);
      }
    } else {
      return new Timer(duration, onStartLoginActivity);
    }
  }
}



abstract class SplashView{
  void onFailed(String errorMessage);
  void onStartLoginActivity();
  void onSelectStoreActivity();
  void onHomeActivity();
  void updateApp(bool isMandatory,String message);
  continueExecution();
}
import 'package:dio/dio.dart';
import 'package:retrivalapp/ui/splash/SplashView.dart';
import 'package:retrivalapp/ui/splash/ValidateAppVersionResponse.dart';
import 'package:retrivalapp/utils/APIConstant.dart';
import 'package:retrivalapp/utils/ApiController.dart';
import 'package:retrivalapp/utils/Message.dart';
import 'package:retrivalapp/utils/NetworkCheck.dart';
import 'package:retrivalapp/utils/Utility.dart';

class SplashPresenter {
  final String TAG = "SplashPresenter";
  SplashView view;
  SplashPresenter(this.view);
  ApiController apiController = new ApiController.internal();

  void validateAppVersion(String versionCode, String appVersion) async {
    if (await NetworkCheck.check()) {
      var xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
          "<version_info>\n " +
          "<version_code>" + versionCode + "</version_code>\n" +
          "<app_type>retrievalapp</app_type>\n" +
          "<version_name>" + appVersion + "</version_name>\n" +
          "</version_info>";
      apiController.postXmlData(APIConstant.validateAppVersion,xml).then((String apiResponse){
        Utility.log(TAG, "Response: "+apiResponse);

        ValidateAppVersionResponse response  = ValidateAppVersionResponse.fromJson(apiResponse);
        if(response.status){
          view.continueExecution();
        }else{
          view.updateApp(response.isMandatory,response.message);
//          view.updateApp(false,response.message);
        }

      }).catchError((error){
        Utility.log(TAG, error.toString());
        view.onFailed(Message.SOMETHING_WENT_WRONG);


      });

    } else {
      view.onFailed(Message.NO_INTERNET);
    }
  }
}

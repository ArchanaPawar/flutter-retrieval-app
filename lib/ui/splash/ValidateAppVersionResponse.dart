// To parse this JSON data, do
//
//     final validateAppVersionResponse = validateAppVersionResponseFromMap(jsonString);

import 'dart:convert';

class ValidateAppVersionResponse {
  ValidateAppVersionResponse({
    this.status,
    this.message,
    this.link,
    this.isMandatory,
  });

  bool status;
  String message;
  String link;
  bool isMandatory;

  factory ValidateAppVersionResponse.fromJson(String str) => ValidateAppVersionResponse.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory ValidateAppVersionResponse.fromMap(Map<String, dynamic> json) => ValidateAppVersionResponse(
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
    link: json["link"] == null ? null : json["link"],
    isMandatory: json["is_mandatory"] == null ? null : json["is_mandatory"],
  );

  Map<String, dynamic> toMap() => {
    "status": status == null ? null : status,
    "message": message == null ? null : message,
    "link": link == null ? null : link,
    "is_mandatory": isMandatory == null ? null : isMandatory,
  };
}
